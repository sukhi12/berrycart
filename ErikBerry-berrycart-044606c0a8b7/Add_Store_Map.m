//
//  Add_Store_Map.m
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import "Add_Store_Map.h"


@implementation Add_Store_Map

@dynamic ad_id;
@dynamic ad_store_map_id;
@dynamic assigned_brand_id;
@dynamic store_id;

@end
