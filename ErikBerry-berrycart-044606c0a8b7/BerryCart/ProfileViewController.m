//
//  ProfileViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/27/14.
//  Copyright (c) 2014 BerryCart.. All rights reserved.
//

#import "ProfileViewController.h"
#import "BerryCart_API.h"

@interface ProfileViewController (){
    
    NSString *selectedGender;
    NSDateFormatter *df;
}

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMMM dd, YYYY"];

}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    [self getUserData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Textfield Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField{
 
    if (textField.tag == 100) {
        
        [UIView animateWithDuration:.15 animations:^{
            self.view.frame = CGRectMake(0, self.view.frame.origin.y - 170, self.view.frame.size.width, self.view.frame.size.height);
        }];
    }
    
}

- (void) textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField.tag == 100) {
        
        [UIView animateWithDuration:.15 animations:^{
            self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        }];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Picker

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    if (pickerView.tag == 10) {
        return 1;
    }else{
        return 1;
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (pickerView.tag == 10) {
        return 2;
    }else{
        return 1;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (row == 0) {
        return @"Male";
    }else{
        return @"Female";
    }
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (pickerView.tag == 10) {
        if (row == 0) {
            selectedGender = @"M";
        }else{
            
            selectedGender = @"F";
        }
    }
}

#pragma mark - Other

- (void) getUserData{
    
    [BerryCart_API getUserData:nil :^(NSDictionary *resul) {
        NSNull *isNull = [NSNull null];
        
        self.emailTextfield.text =resul[@"data"][@"email"] != isNull ? resul[@"data"][@"email"] : @"";
        
        self.firstNameTextfield.text = resul[@"data"][@"first_name"] != isNull ? resul[@"data"][@"first_name"] : @"";
        
        self.lastNameTextfield.text = resul[@"data"][@"last_name"] != isNull ? resul[@"data"][@"last_name"] : @"";
        
        self.genderTextfield.text =resul[@"data"][@"sex"] != isNull ? resul[@"data"][@"sex"] : @"";
        
        //[df setDateFormat:@"MM-dd-yyyy"];
        [df setDateFormat:@"yyyy-MM-dd"];
        
        NSString *date = resul[@"data"][@"birthdate"] != isNull ? resul[@"data"][@"birthdate"] : @"";
        NSLog(@"date :%@", date);
        
        NSDate *dateString = [df dateFromString:date];
        NSLog(@"dateString :%@",dateString);
        
        [df setDateFormat:@"MMMM dd, YYYY"];

        NSString *finalDate = [df stringFromDate:dateString];
        NSLog(@"finalDate :%@", finalDate);
        
        self.dateTextfield.text = finalDate;
        
        self.zipcodeTextfield.text = resul[@"data"][@"zip_code"] != isNull ? resul[@"data"][@"zip_code"] : @"";
    }];
}

- (IBAction)genderTapped:(UIButton *)sender {
    
    for (UITextField *oneTextfield in self.view.subviews) {
        
        [oneTextfield resignFirstResponder];
        
    }
    
    self.genderView.hidden = NO;
}

- (IBAction)saveTapped:(UIButton *)sender {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:self.emailTextfield.text, @"email", self.firstNameTextfield.text, @"first_name", self.lastNameTextfield.text, @"last_name", self.datePicker.date, @"birthdate", self.genderTextfield.text , @"sex", self.zipcodeTextfield.text, @"zip_code", nil];
    
    [BerryCart_API updateUser:params :^(NSDictionary *resul) {
        
        NSLog(@"result :%@", resul);
        
        if ([resul[@"message"] isEqualToString:@"success"]) {
            [[[UIAlertView alloc] initWithTitle:@"Success" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        
    }];
}

- (IBAction)dobTapped:(UIButton *)sender {
    
    for (UITextField *oneTextfield in self.view.subviews) {
        
        [oneTextfield resignFirstResponder];
        
    }
    
    [self.datePicker setMinimumDate:[NSDate date]];
    
    self.dateView.hidden = NO;

}

- (IBAction)doneTapped:(UIBarButtonItem *)sender {
    
    if (sender.tag == 10) {
        
        if (selectedGender == nil) {
            self.genderTextfield.text = @"M";
        }else{
            self.genderTextfield.text = selectedGender;
        }
        
    }else{
        
        self.dateTextfield.text = [df stringFromDate:self.datePicker.date];
    }
    
    self.genderView.hidden = YES;

    self.dateView.hidden = YES;

}
@end
