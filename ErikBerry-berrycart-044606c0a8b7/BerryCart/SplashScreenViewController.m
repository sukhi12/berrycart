//
//  SplashScreenViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/27/14.
//  Copyright (c) 2014 BerryCart.. All rights reserved.
//

#import "SplashScreenViewController.h"
#import "RegisterViewController.h"

@interface SplashScreenViewController ()

@end

@implementation SplashScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIView* nibView;
    
    if (self.view.frame.size.height > 480) { // iPhone 5
        
        nibView = [[NSBundle mainBundle] loadNibNamed:@"SplashScreenView"
                                                        owner:self
                                                      options:nil][0];
    }else{
        
        nibView = [[NSBundle mainBundle] loadNibNamed:@"SplashScreenView"
                                                owner:self
                                              options:nil][1];

    }
    
    [self.scrollView addSubview:nibView];
    [self.scrollView setContentSize:CGSizeMake(nibView.frame.size.width, nibView.frame.size.height)];
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden = YES;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    self.navigationController.navigationBarHidden = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signInTapped:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"toLogin" sender:nil];
}

- (IBAction)createAcc:(UIButton *)sender {

    [self performSegueWithIdentifier:@"toRegister" sender:nil];
    
}
@end
