//
//  ScanBarcodeViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Ad.h"
#import "Action.h"

@interface ScanBarcodeViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong)Ad *selectedAd;

@property (weak, nonatomic) IBOutlet UIView *scannerView;
@property (weak, nonatomic) IBOutlet UIImageView *styleImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

//for completion with receipt and barcode

@property (weak, nonatomic) IBOutlet UILabel *statusBar;
@property (nonatomic,strong) NSArray *receiptImageArray;
@property (nonatomic,strong) NSArray *actionCompletionID;



- (IBAction)goBack:(UIButton *)sender;

@end
