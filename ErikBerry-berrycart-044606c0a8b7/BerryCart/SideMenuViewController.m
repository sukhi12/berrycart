//
//  SideMenuViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SideMenuCell.h"
#import "SWRevealViewController.h"
#import "PickAStoreViewController.h"

@interface SideMenuViewController (){
    
    NSMutableArray *namesArray;
}

@end

@implementation SideMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    namesArray = [NSMutableArray arrayWithObjects:@"Healthy Deals", @"Verify Purchase", @"Earnings", @"Settings", @"Help", nil];
    
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return namesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSString *imageName = [NSString stringWithFormat:@"Menu%i",indexPath.row + 1];
    
    cell.logo.image = [UIImage imageNamed:imageName];
    
    cell.nameLabel.text = namesArray[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"toHealthyDeals" sender:nil];
    }else if (indexPath.row == 1){
        [self performSegueWithIdentifier:@"toVerifyPurchase" sender:nil];
    }else if (indexPath.row == 2) {
        [self performSegueWithIdentifier:@"toEarnings" sender:self];
    }else if (indexPath.row == 3){
        [self performSegueWithIdentifier:@"toSettings" sender:nil];
    }else if (indexPath.row == 4){
        [self performSegueWithIdentifier:@"toHelp" sender:nil];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    SWRevealViewControllerSegue* rvcs = (SWRevealViewControllerSegue*) segue;
    
    SWRevealViewController* rvc = self.revealViewController;
    NSAssert( rvc != nil, @"oops! must have a revealViewController" );
    
    NSAssert( [rvc.frontViewController isKindOfClass: [UINavigationController class]], @"oops!  for this segue we want a permanent navigation controller in the front!" );
    
    rvcs.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc)
    {
        [rvc setFrontViewController:segue.destinationViewController animated:YES];
    };
    
}

@end
