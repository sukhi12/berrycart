//
//  HelpViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/27/14.
//  Copyright (c) 2014 BerryCart.. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIWebView *webview;

@end
