//
//  PopUpViewController.m
//  BerryCart
//
//  Created by kristopher Amiel Reyes on 2/7/14.
//  Copyright (c) 2014 Codemagnus. All rights reserved.
//

#import "PopUpViewController.h"

@interface PopUpViewController ()

@end

@implementation PopUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CallPopupWithImage:(UIImage *)imagename withView:(UIView *)view automaticClose:(BOOL)isautomatic withDelay:(float)interval withHeight:(float)height withwidth:(float)width{
    
    UIImageView *thisImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    thisImageView.image = imagename;
    thisImageView.center = view.center;
    self.darkView = [[UIView alloc]initWithFrame:view.frame];
    self.darkView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.8f];
    self.darkView.alpha = 0.0f;
    
    [view addSubview:self.darkView];
    [view addSubview:self.view];
    [self animateLikeCrazy:self.view automaticClose:isautomatic withDelay:interval];
}


-(void)callCompletedWithRootView:(UIView *)view automaticClose:(BOOL)isautomatic withDelay:(float)interval{
    
    self.view.center = view.center;
    self.darkView = [[UIView alloc]initWithFrame:view.frame];
    self.darkView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.8f];
    self.darkView.alpha = 0.0f;
    
    [view addSubview:self.darkView];
    [view addSubview:self.view];
    [self animateLikeCrazy:self.view automaticClose:isautomatic withDelay:interval];
}

- (void)animateLikeCrazy:(UIView *)animateView automaticClose:(BOOL)isautomatic withDelay:(float)interval{
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        if (self.animationIndicator == NO) {
            self.animationIndicator = YES;
            animateView.transform =CGAffineTransformMakeScale(1.1f, 1.1f);
            self.darkView.alpha = 1.0f;
        }
    } completion:^(BOOL finished) {
        if (finished) {
            self.animationIndicator = NO;
            [UIView animateWithDuration:0.15 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                if (self.animationIndicator == NO) {
                    self.animationIndicator = YES;
                    animateView.transform =CGAffineTransformMakeScale(0.9f, 0.9f);
                }
            } completion:^(BOOL finished) {
                if (finished) {
                    self.animationIndicator = NO;
                    [UIView animateWithDuration:0.10 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                        if (self.animationIndicator == NO) {
                            self.animationIndicator = YES;
                            animateView.transform =CGAffineTransformMakeScale(1, 1);
                        }
                    } completion:^(BOOL finished) {
                        if (finished) {
                            self.animationIndicator = NO;
                        }
                    }];
                }
            }];
        }
        
        if (isautomatic == YES) {
            [self performSelector:@selector(ExitAnimationWithView) withObject:nil afterDelay:interval];
        }
        
        
    }];
}

-(void)ExitAnimationWithView{
    
    [UIView animateWithDuration:0.30 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        if (self.animationIndicator == NO) {
            self.animationIndicator = YES;
            self.view.alpha = 0;
            self.darkView.alpha = 0.0f;
        }
    } completion:^(BOOL finished) {
        if (finished) {
            self.animationIndicator = NO;
            [self.view removeFromSuperview];
            [self.darkView removeFromSuperview];
            self.view = nil;
            self.darkView = nil;
            
            
            if([_delegate respondsToSelector:@selector(PopupDidclose)]){
                [_delegate PopupDidclose];
            }
        }
    }];
    
}



@end
