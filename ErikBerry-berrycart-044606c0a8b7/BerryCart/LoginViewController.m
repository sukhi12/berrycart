//
//  LoginViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "LoginViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "BerryCart_API.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"

@interface LoginViewController (){
    SWRevealViewController *SWRVC;
    MBProgressHUD *HUD;
}

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    SWRVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    
    for (UIView *view in self.view.subviews) {
        
        if (![view isKindOfClass:[UIButton class]] && ![view isKindOfClass:[UILabel class]] && ![view isKindOfClass:[UIImageView class]]) {
            
            view.layer.cornerRadius = 4;
            view.layer.masksToBounds = YES;
            view.layer.shadowColor = [UIColor grayColor].CGColor;
            view.layer.borderWidth = .5;
            
        }
        
    }
    
    for (UIView *view in self.view1.subviews) {
        
        if (![view isKindOfClass:[UIButton class]]) {
            
            if (view.tag != 10) {
                
                view.layer.cornerRadius = 4;
                view.layer.masksToBounds = YES;
                view.layer.shadowColor = [UIColor grayColor].CGColor;
                view.layer.borderWidth = .5;

            }
            
        }
        
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 135, 33)];
    imageView.image = [UIImage imageNamed:@"logo"];
    self.navigationItem.titleView = imageView;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginViaFacebook:) name:@"SuccessLogin" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideHUD) name:@"FacebookLoginFailed" object:nil];
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
     NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    self.emailAddressOutlet.text=[defaults valueForKey:@"RemailId"];
    self.passwordOutlet.text=[defaults valueForKey:@"Rpassword"];
    
     [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationController.navigationBarHidden = NO;
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hideHUD{
    
    NSLog(@"hide the hud if user cancels fb login");
    [[[UIAlertView alloc] initWithTitle:@"Ooops" message:@"Berry Cart is not allowed to use your basic profile info. Please go to settings to allow it." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    [MBProgressHUD hideHUD];

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}



- (IBAction)loginTapped:(UIButton *)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    [defaults setValue:_emailAddressOutlet.text forKey:@"RemailId"];
    [defaults setValue:_passwordOutlet.text forKey:@"Rpassword"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [BerryCart_API registerDeviceAction:^(BOOL isRegistered) {
        
        if (isRegistered) {
            if (![BerryCart_API user_session]){
                //create sesion
                [BerryCart_API createSessionAction:^(BOOL hasSession) {
                    if (hasSession) {
                        [self LoginActionWith];
                    }
                }];
            }else{
                [self LoginActionWith];
            }
        }
    }];
}



-(void)LoginActionWith{
    
    [BerryCart_API loginAction:[NSDictionary dictionaryWithObjects:@[[BerryCart_API user_session],self.emailAddressOutlet.text,self.passwordOutlet.text,@"login"] forKeys:@[@"session",@"email",@"pin",@"action"]] completion:^(NSDictionary *resul) {
        
        [theAppDelegate.loggedInUser setValue:self.emailAddressOutlet.text forKey:@"email"];

        if ([[NSString stringWithFormat:@"%@",resul[@"message"]] isEqualToString:@"success"]) {
            
             [self presentViewController:SWRVC animated:YES completion:nil];
            
        }else if ([[NSString stringWithFormat:@"%@",resul[@"messege"]] isEqualToString:@""]){
            
        }else{
            
        }
        
    }];
}

- (void) loginViaFacebook: (NSNotification *)notif{
    
    [BerryCart_API createSessionAction:^(BOOL hasSession) {
        
        if (hasSession) {

            [BerryCart_API addFacebookID:notif.object[@"id"]];
            
            [BerryCart_API loginAction:@{@"session":[BerryCart_API user_session], @"facebook_id":[BerryCart_API facebookID]} completion:^(NSDictionary *resul) {
                [MBProgressHUD hideHUD];

                
                NSLog(@"123result :%@", resul);

                
                if ([resul[@"message"] isEqualToString:@"customer does not exist"]) {

                    
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Create A BerryCart Account" message:@"You need to create a BerryCart" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertView show];
                    
                    //push to the account creation view
                    //   [self createAccount:nil];
                    
                    [self performSegueWithIdentifier:@"toCreateAccount" sender:nil];

                    
                    
                }else if ([[NSString stringWithFormat:@"%@",resul[@"message"]] isEqualToString:@"success"]){
                    
                    
                    [theAppDelegate.loggedInUser setValue:self.emailAddressOutlet.text forKey:@"email"];

                    [self presentViewController:SWRVC animated:YES completion:nil];

                }

            }];
            
            
        }else{
            
            
        }
        
    }];
    
    
    
   

}

- (IBAction)logInToFacebook:(UIButton *)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    theAppDelegate.fromLogin = YES;
    NSLog(@"return %i",[theAppDelegate openSessionWithAllowLoginUI:YES]);
    
}
@end
