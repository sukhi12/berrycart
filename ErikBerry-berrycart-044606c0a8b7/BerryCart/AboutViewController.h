//
//  AboutViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/31/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController
- (IBAction)buttonsTapped:(UIButton *)sender;

@end
