//
//  SplashScreenViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/27/14.
//  Copyright (c) 2014 BerryCart.. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashScreenViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)signInTapped:(UIButton *)sender;
- (IBAction)createAcc:(UIButton *)sender;


@end
