//
//  AboutViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/31/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import "AboutViewController.h"
#import "WebViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonsTapped:(UIButton *)sender {
    WebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    if (sender.tag == 1) {
        webViewController.url = @"http://berrycart.com/Privacy.html";
    }else if (sender.tag == 2) {
        webViewController.url = @"http://berrycart.com/Terms.html";
    }else if (sender.tag == 3) {
        webViewController.url = @"http://berrycart.com/EULA-iOS.html";
    }else if (sender.tag == 4) {
        
        
    }
    
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
}

@end
