//
//  FacebookViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ad.h"
#import "Action.h"
#import "PopUpViewController.h"

@interface FacebookViewController : UIViewController<PopUpDelegate>

@property (nonatomic, strong) Ad *data;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceToBeEarned;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) Action *action;

- (IBAction)goBack:(UIButton *)sender;
- (IBAction)postToFacebook:(UIButton *)sender;

@end
