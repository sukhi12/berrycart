//
//  EarningsViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EarningsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *earningScrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIButton *transferToPaypal;
@property (weak, nonatomic) IBOutlet UIButton *verifyPurchase;
@property (weak, nonatomic) IBOutlet UILabel *earnedLabel;
@property (weak, nonatomic) IBOutlet UILabel *pendingLabel;

- (IBAction)goBack:(UIButton *)sender;
- (IBAction)buttonsTapped:(UIButton *)sender;

@end
