//
//  WhereToBuyViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "WhereToBuyViewController.h"

@interface WhereToBuyViewController (){
    NSMutableArray *storeMArray;
}

@end

@implementation WhereToBuyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.mapKit.delegate = self;
    storeMArray = [@[] mutableCopy];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    

    [self renderstorePin];
    
}

-(void)renderstorePin{
    
    NSArray *fetchedObjects = [UpdateAllData fetchwithEntityName:@"Add_Store_Map" withPredicateKey:@"ad_id" valueToSearch:[NSString stringWithFormat:@"%@",self.selectedAd.ad_id]];
    
    [storeMArray removeAllObjects];
    
    [fetchedObjects enumerateObjectsUsingBlock:^(Add_Store_Map *storemap, NSUInteger idx, BOOL *stop) {
        
        NSArray *storeArray = [UpdateAllData fetchwithEntityName:@"Store" withPredicateKey:@"store_id" valueToSearch:[NSString stringWithFormat:@"%@",storemap.store_id]];
        
        Store *thisStore = storeArray[0];
        
        [storeMArray  addObject:thisStore];
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        
        [annotation setCoordinate:CLLocationCoordinate2DMake([thisStore.latitude floatValue], [thisStore.longitude floatValue])];
        [annotation setTitle:[NSString stringWithFormat:@"%@",thisStore.store_name]];
        [annotation setSubtitle:[NSString stringWithFormat:@"%@",thisStore.address]];
        
        [self.mapKit addAnnotation:annotation];
        
        
    }];
    
    NSLog(@"storeMArray %i",storeMArray.count);
    
    [self.storeTableView reloadData];
    

}
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    
}


- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation{
    
    
    static NSString *SFAnnotationIdentifier = @"SFAnnotationIdentifier";
    MKPinAnnotationView *pinView =
    (MKPinAnnotationView *)[theMapView dequeueReusableAnnotationViewWithIdentifier:SFAnnotationIdentifier];
    if (!pinView)
    {
        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                         reuseIdentifier:SFAnnotationIdentifier];
        annotationView.image = [UIImage imageNamed:@"fullPIN"];
        annotationView.canShowCallout = YES;

        
        return annotationView;
    }
    else
    {
        pinView.annotation = annotation;
    }
    return pinView;
}


- (IBAction)listAndMapButton:(UIButton *)sender {
    
    if (sender.tag == 0) {
        
        self.storeTableView.hidden = NO;
        self.mapKit.hidden = YES;
        
    }else{
        
        self.storeTableView.hidden = YES;
        self.mapKit.hidden = NO;
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"tableview");
    return storeMArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    wheretobuyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"wheretobuyCell" forIndexPath:indexPath];
    Store *selectedStore = storeMArray[indexPath.row];
    
    
    cell.storename.text = selectedStore.store_name;

    
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 42;
}





- (IBAction)goBack:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES]; 
}
@end
