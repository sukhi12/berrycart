//
//  WebViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/31/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSString *url;

@end
