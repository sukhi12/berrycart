//
//  HomeViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
@import CoreLocation;


@interface HomeViewController : UIViewController <SWRevealViewControllerDelegate, UITableViewDataSource, UITableViewDataSource,CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UITableView *homeTableView;


@end
