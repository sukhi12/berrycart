//
//  WatchVideoViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "LBYouTubePlayerViewController.h"
#import "PopUpViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "Action.h"

@interface WatchVideoViewController : UIViewController<LBYouTubePlayerControllerDelegate,LBYouTubeExtractorDelegate,PopUpDelegate>{
    MPMoviePlayerViewController *moviePlayerViewController;
    LBYouTubePlayerViewController *lbYoutubePlayerVC;

}
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (nonatomic,strong)Action *action;
@property (weak, nonatomic) IBOutlet UIImageView *youtubeThumbNail;
@property (weak, nonatomic) IBOutlet UILabel *earnValue;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

- (IBAction)goBack:(UIButton *)sender;
- (IBAction)playButton:(UIButton *)sender;
- (IBAction)submitButton:(UIButton *)sender;

@end
