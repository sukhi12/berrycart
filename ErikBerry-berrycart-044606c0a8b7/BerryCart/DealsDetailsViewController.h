//
//  DealsDetailsViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Ad.h"

@interface DealsDetailsViewController : UIViewController <SWRevealViewControllerDelegate>

@property (nonatomic,strong)Ad *selectedAd;
@property (nonatomic, strong) NSDictionary *data;

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productDescription;


@property (weak, nonatomic) IBOutlet UIScrollView *actionScrollView;

//Value
@property (weak, nonatomic) IBOutlet UILabel *videoValue;
@property (weak, nonatomic) IBOutlet UILabel *polValue;
@property (weak, nonatomic) IBOutlet UILabel *facebookValue;
@property (weak, nonatomic) IBOutlet UILabel *viewFactValue;
@property (weak, nonatomic) IBOutlet UILabel *shareOnTwitterValue;
@property (weak, nonatomic) IBOutlet UILabel *scanCheckInValue;
@property (weak, nonatomic) IBOutlet UILabel *expirationLabel;

//button outlet
@property (weak, nonatomic) IBOutlet UIButton *videoButton;
@property (weak, nonatomic) IBOutlet UIButton *pollButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *factView;
@property (weak, nonatomic) IBOutlet UIButton *shareOnTwiter;
@property (weak, nonatomic) IBOutlet UIButton *scanCheckin;
@property (weak, nonatomic) IBOutlet UIButton *redeemButton;

//instant label
@property (weak, nonatomic) IBOutlet UILabel *facebookInstant;
@property (weak, nonatomic) IBOutlet UILabel *watchInstant;
@property (weak, nonatomic) IBOutlet UILabel *pollInstant;
@property (weak, nonatomic) IBOutlet UILabel *factInstant;
@property (weak, nonatomic) IBOutlet UILabel *shareOnTwitterInstant;
@property (weak, nonatomic) IBOutlet UILabel *scanCheckInInstant;



//Constraint

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pollConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *watchConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *facebookConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *factConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *twitterConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scanCheckInConstraint;


//Button action

- (IBAction)goBack:(UIButton *)sender;
- (IBAction)redeemTapped:(UIButton *)sender;
- (IBAction)pollTapped:(UIButton *)sender;
- (IBAction)watchVideo:(UIButton *)sender;
- (IBAction)shareOnFacebook:(UIButton *)sender;
- (IBAction)viewFactAction:(UIButton *)sender;
- (IBAction)shareOnWitterAction:(UIButton *)sender;
- (IBAction)scanCheckinAction:(UIButton *)sender;

@end
