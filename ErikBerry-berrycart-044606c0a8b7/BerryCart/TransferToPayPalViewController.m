//
//  TransferToPayPalViewController.m
//  BerryCart
//
//  Created by Rommel Bulalacao on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "TransferToPayPalViewController.h"
#import "AppDelegate.h"
#import "BerryCart_API.h"

@interface TransferToPayPalViewController ()

@end

@implementation TransferToPayPalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.textfieldView.layer.cornerRadius = 4;
    self.textfieldView.layer.masksToBounds = YES;
    self.textfieldView.layer.shadowColor = [UIColor grayColor].CGColor;
    self.textfieldView.layer.borderWidth = .5;

}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    self.emailAddressLabel.text = self.emailAddress;
    
    /*    [PayPalPaymentViewController setEnvironment:PayPalEnvironmentNoNetwork];
    [PayPalPaymentViewController prepareForPaymentUsingClientId:@"AZbmTRBZb2CZ2BJWn47wgCwfUkxEsHOG1bI6gU6dOo_HBQ7cexKoYPkJA4xD"];*/
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (self.view.frame.size.height > 480) {
        
    }else{
        
        [UIView animateWithDuration:.25 animations:^{
            self.view.frame = CGRectMake(0, self.view.frame.origin.y - 40, self.view.frame.size.width, self.view.frame.size.height);
        }];

    }
    
}

- (void) textFieldDidEndEditing:(UITextField *)textField{
    
    if (![textField.text hasPrefix:@"."]){
        textField.text = [NSString stringWithFormat:@"%@.00", textField.text];
    }
    
    if (self.view.frame.size.height > 480) {
        
    }else{
        
        [UIView animateWithDuration:.25 animations:^{
            self.view.frame = CGRectMake(0, self.view.frame.origin.y + 40, self.view.frame.size.width, self.view.frame.size.height);
        }];
        
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField.text.length == 0) {
        textField.text = @"$";
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)goBack:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)transferFundsToPaypal:(UIButton *)sender {

    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    [parameter setObject:@"pay_amount" forKey:@"action"];
    [parameter setObject:[BerryCart_API getUDID] forKey:@"session"];
    
    NSNumber *value = [NSNumber numberWithFloat:[[self.amountTextfield.text substringFromIndex:1] floatValue] * 100];
    [parameter setObject:value forKey:@"transfer_amt"];
    
    [BerryCart_API transferFundToPayPal:parameter :^(NSDictionary *resul) {
     
        NSLog(@"result :%@", resul);
        
    }];

    
}

@end
