//
//  SurveyCell.m
//  BerryCart
//
//  Created by Romeo Flauta on 2/3/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import "SurveyCell.h"

@implementation SurveyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
