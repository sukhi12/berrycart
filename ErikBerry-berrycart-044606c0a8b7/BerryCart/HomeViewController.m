//
//  HomeViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "HomeViewController.h"
#import "Ad.h"
#import "Action.h"
#import "MBProgressHUD.h"

#import "HomeCell.h"
#import "DealsDetailsViewController.h"
#import "BerryCart_API.h"
#import "AppDelegate.h"

@interface HomeViewController (){
    
    NSMutableArray *temporaryArray;
    
    NSMutableArray *adMArray;
    NSTimer *adTimer;
    NSDateFormatter *f;
    NSNull *isNUll;
    CLLocationManager *lm;
}
@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    lm = [[CLLocationManager alloc] init];
    lm.delegate = self;
    lm.desiredAccuracy = kCLLocationAccuracyBest;
    lm.distanceFilter = kCLDistanceFilterNone;

    
    temporaryArray = [NSMutableArray array];
    adMArray = [NSMutableArray array];
    f =[[NSDateFormatter alloc]init];
    [f setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    isNUll = [NSNull null];
    
    // Side Menu
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
  //  adTimer = [NSTimer scheduledTimerWithTimeInterval:3600 target:self selector:@selector(get_sync_data) userInfo:nil repeats:YES];

}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
//    [self get_sync_data];
    [lm startUpdatingLocation];
    

}
-(void)get_sync_data{
    
/**
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Ad" inManagedObjectContext:theAppDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [theAppDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    adMArray = [NSMutableArray arrayWithArray:fetchedObjects];

    
    [BerryCart_API updateAdOrDeals:^(NSDictionary *dict) {
        
        [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *addDcit, NSUInteger idx, BOOL *stop) {
            
            NSPredicate *predicateID = [NSPredicate predicateWithFormat:@"ad_id = %i",[dict[@"id"] intValue]];
            NSArray *arrayWithPredicate = [fetchedObjects filteredArrayUsingPredicate:predicateID];
            
            if (arrayWithPredicate.count == 0) {
                
                Ad *addAd = [NSEntityDescription insertNewObjectForEntityForName:@"Ad" inManagedObjectContext:theAppDelegate.managedObjectContext];
                
                addAd.activation_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"activation_date"]]];
                addAd.ad_id = [NSNumber numberWithInt:[addDcit[@"id"]intValue]];
                addAd.customer_segment = addDcit[@"segment_id"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"segment_id"]intValue]] : [NSNumber numberWithInt:0];
                addAd.expiration_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"expiration_date"]]];
                addAd.image = addDcit[@"image"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"image"]] : @"";
                addAd.line1 = addDcit[@"line1"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line1"]] : @"";
                addAd.line2 = addDcit[@"line2"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line2"]] : @"";
                addAd.max_uses = addDcit[@"max_uses"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses"]intValue]] : [NSNumber numberWithInt:0];
                addAd.max_uses_per_day = addDcit[@"max_uses_per_day"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses_per_day"]intValue]] : [NSNumber numberWithInt:0];
                addAd.product_UPCs = addDcit[@"product_UPCs"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"product_UPCs"]] : @"";
                addAd.title = addDcit[@"title"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"title"]] : @"";
                [adMArray addObject:addAd];
                
            }else{
                
                [arrayWithPredicate enumerateObjectsUsingBlock:^(Ad *addAd, NSUInteger idx, BOOL *stop) {
                    
                    addAd.activation_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"activation_date"]]];
                    addAd.ad_id = [NSNumber numberWithInt:[addDcit[@"id"]intValue]];
                    addAd.customer_segment = addDcit[@"segment_id"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"segment_id"]intValue]] : [NSNumber numberWithInt:0];
                    addAd.expiration_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"expiration_date"]]];
                    addAd.image = addDcit[@"image"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"image"]] : @"";
                    addAd.line1 = addDcit[@"line1"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line1"]] : @"";
                    addAd.line2 = addDcit[@"line2"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line2"]] : @"";
                    addAd.max_uses = addDcit[@"max_uses"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses"]intValue]] : [NSNumber numberWithInt:0];
                    addAd.max_uses_per_day = addDcit[@"max_uses_per_day"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses_per_day"]intValue]] : [NSNumber numberWithInt:0];
                    addAd.product_UPCs = addDcit[@"product_UPCs"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"product_UPCs"]] : @"";
                    addAd.title = addDcit[@"title"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"title"]] : @"";
                    
                }];
            }
            
            [theAppDelegate saveContext];
            
        }];

        [self.homeTableView reloadData];
    }];
    
    [self.homeTableView reloadData];
    */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return adMArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Ad *thisProduct = adMArray[indexPath.row];
    NSString *thisImageName = [NSString stringWithFormat:@"Test Product %i.png", indexPath.row + 1];
    
    // Configure the cell...

    cell.productNameLabel.text = thisProduct.title;
  __block float earn  = 0;
    
    
    [thisProduct.actions enumerateObjectsUsingBlock:^(Action *obj, BOOL *stop) {
       earn += [obj.offer_value floatValue];
    }];
    
    
    cell.detailsLabel.text = thisProduct.line1;
    cell.earnLabel.text = [NSString stringWithFormat:@"$ %0.2f",earn];
    cell.productImageView.image = [UIImage imageNamed:thisImageName];
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];


    Ad *oneAd = adMArray[indexPath.row];
    
    [BerryCart_API getAdByID:[NSString stringWithFormat:@"%@",oneAd.ad_id] completaion:^(NSDictionary *dict) {
        [MBProgressHUD hideHUD];
        
        NSTimeInterval interval = [dict[@"server_timestamp"] doubleValue];
        NSDate *today = [NSDate dateWithTimeIntervalSince1970:interval];
        NSDate *expirationDate = [f dateFromString:dict[@"data"][0][@"expiration_date"]];
        
        if ([today compare:expirationDate] == NSOrderedAscending) {
            
            if ([[NSString stringWithFormat:@"%@",dict[@"data"][0][@"deleted_flag"]] isEqualToString:@"1"]) {// ad is deleted
                
                [[[UIAlertView alloc]initWithTitle:@"Deleted" message:@"This ad has recently been deleted." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
                
                [theAppDelegate.managedObjectContext deleteObject:oneAd];
                [adMArray removeObject:oneAd];
                [theAppDelegate saveContext];
                [self.homeTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                
            }else{
                oneAd.image = [NSString stringWithFormat:@"Test Product %i.png", indexPath.row + 1];
                [self performSegueWithIdentifier:@"toDealsDescription" sender:oneAd];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
            }
        }else{
            
            
            [theAppDelegate.managedObjectContext deleteObject:oneAd];
            [adMArray removeObject:oneAd];
            [theAppDelegate saveContext];
            [self.homeTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
        }
    }];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    [lm stopUpdatingLocation];
    CLLocation *location = [lm location];
    
    NSLog(@"location latitude %f location latitude %f",location.coordinate.latitude,location.coordinate.longitude);
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Ad" inManagedObjectContext:theAppDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [theAppDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    
    [BerryCart_API updateAdOrDealsWithLongLat:location completion:^(NSDictionary *dict) {
        [adMArray removeAllObjects];
        
        NSLog(@"dict dict dict: %@", dict);

        
        [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *addDcit, NSUInteger idx, BOOL *stop) {
            
            NSPredicate *predicateID = [NSPredicate predicateWithFormat:@"ad_id == %i",[addDcit[@"id"] intValue]];
            NSArray *arrayWithPredicate = [fetchedObjects filteredArrayUsingPredicate:predicateID];
            NSLog(@"arrayWithPredicate %i",arrayWithPredicate.count);
            
            if (arrayWithPredicate.count == 0) {
                
                Ad *addAd = [NSEntityDescription insertNewObjectForEntityForName:@"Ad" inManagedObjectContext:theAppDelegate.managedObjectContext];
                
                addAd.activation_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"activation_date"]]];
                addAd.ad_id = [NSNumber numberWithInt:[addDcit[@"id"]intValue]];
                addAd.customer_segment = addDcit[@"segment_id"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"segment_id"]intValue]] : [NSNumber numberWithInt:0];
                addAd.expiration_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"expiration_date"]]];
                addAd.image = addDcit[@"image"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"image"]] : @"";
                addAd.line1 = addDcit[@"line1"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line1"]] : @"";
                addAd.line2 = addDcit[@"line2"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line2"]] : @"";
                addAd.max_uses = addDcit[@"max_uses"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses"]intValue]] : [NSNumber numberWithInt:0];
                addAd.max_uses_per_day = addDcit[@"max_uses_per_day"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses_per_day"]intValue]] : [NSNumber numberWithInt:0];
                addAd.product_UPCs = addDcit[@"product_UPCs"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"product_UPCs"]] : @"";
                addAd.title = addDcit[@"title"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"title"]] : @"";
                [adMArray addObject:addAd];
                
            }else{
                
                
                
                
             //   [arrayWithPredicate enumerateObjectsUsingBlock:^(Ad *addAd, NSUInteger idx, BOOL *stop) {
                    Ad *addAd = arrayWithPredicate[0];
                        addAd.activation_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"activation_date"]]];
                        addAd.ad_id = [NSNumber numberWithInt:[addDcit[@"id"]intValue]];
                        addAd.customer_segment = addDcit[@"segment_id"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"segment_id"]intValue]] : [NSNumber numberWithInt:0];
                        addAd.expiration_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"expiration_date"]]];
                        addAd.image = addDcit[@"image"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"image"]] : @"";
                        addAd.line1 = addDcit[@"line1"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line1"]] : @"";
                        addAd.line2 = addDcit[@"line2"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line2"]] : @"";
                        addAd.max_uses = addDcit[@"max_uses"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses"]intValue]] : [NSNumber numberWithInt:0];
                        addAd.max_uses_per_day = addDcit[@"max_uses_per_day"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses_per_day"]intValue]] : [NSNumber numberWithInt:0];
                        addAd.product_UPCs = addDcit[@"product_UPCs"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"product_UPCs"]] : @"";
                        addAd.title = addDcit[@"title"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"title"]] : @"";
                        if (![adMArray containsObject:addAd]) {
                            [adMArray addObject:addAd];
                        
                    }
                    
                  

           //     }];
            }
            
            [theAppDelegate saveContext];
            
        }];
        
        [self.homeTableView reloadData];
    }];
   
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"toDealsDescription"]) {
        
        DealsDetailsViewController *dealsDetailsViewController = segue.destinationViewController;
        
        Ad *data = sender;
        
        dealsDetailsViewController.selectedAd = data;
        
    }
}

#pragma mark - Other Methods


@end
