//
//  DealsDetailsViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "DealsDetailsViewController.h"
#import "FacebookViewController.h"
#import "AppDelegate.h"
#import "BerryCart_API.h"
#import "Action.h"
#import "PickAStoreViewController.h"
#import "SurveyViewController.h"
#import "Action_Rule.h"
#import "WatchVideoViewController.h"
#import "PickAStoreViewController.h"
#import "WhereToBuyViewController.h"


@interface DealsDetailsViewController (){
    NSDateFormatter *expirationFormatter;
    WatchVideoViewController *watchVideoVC;
    PickAStoreViewController *picAstoreVC;
    BOOL reedemButtonIsactive;
    NSMutableArray *acitonCompletionID;
}

@end

@implementation DealsDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    acitonCompletionID = [@[] mutableCopy];
    expirationFormatter = [[NSDateFormatter alloc]init];
    [expirationFormatter setDateFormat:@"MM-dd-yyyy"];
    picAstoreVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PickAStoreViewController"];
    // Side Menu
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"actionsView" owner:self options:nil];
    UIView *xibActionView = (UIView *)viewArray[0];

    [self.actionScrollView addSubview:xibActionView];
    [self.actionScrollView setContentSize:CGSizeMake(xibActionView.frame.size.width, xibActionView.frame.size.height)];
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    //button outlet
    self.videoButton.enabled = NO;
    self.pollButton.enabled = NO;
    self.shareButton.enabled = NO;
    
    self.videoButton.hidden = YES;
    self.pollButton.hidden = YES;
    self.shareButton.hidden = YES;
    
    self.facebookValue.hidden = YES;
    self.videoValue.hidden =  YES;
    self.polValue.hidden = YES;
    
    self.facebookInstant.hidden = YES;
    self.watchInstant.hidden = YES;
    self.pollInstant.hidden = YES;
    
    
    //Labels for Value
    self.facebookValue.text = @"$0.00";
    self.videoValue.text =  @"$0.00";
    self.polValue.text = @"$0.00";

    [self loadData];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadData{
    
    [BerryCart_API checkactionByUserID:theAppDelegate.loggedInUser[@"data"][@"Debug"][@"CustomerId"] completaion:^(NSDictionary *dict) {
        
        [acitonCompletionID removeAllObjects];
        self.productImageView.image = [UIImage imageNamed:self.selectedAd.image];
        self.productNameLabel.text = self.selectedAd.title;
        self.productDescription.text = [NSString stringWithFormat:@"%@\n%@", self.selectedAd.line1, self.selectedAd.line2];
        self.expirationLabel.text = [expirationFormatter stringFromDate:self.selectedAd.expiration_date];
        NSMutableArray *arrayofnumber = [NSMutableArray array];
        
        [self.selectedAd.actions enumerateObjectsUsingBlock:^(Action *action, BOOL *stop) {
            
         __block BOOL isCompleted = NO;
            
            for (NSDictionary *oneDict in (NSArray *)dict[@"data"]) {
                if ([action.action_id isEqualToNumber:[NSNumber numberWithInt:[oneDict[@"action_id"]intValue]]]) {
                    [acitonCompletionID  addObject:oneDict[@"id"]];
                    isCompleted = YES;
                    break;
                }
            }
            
            if(isCompleted){

                if ([action.action_type isEqualToString:@"Facebook Share"]) {
                    
                    if ([action.validation_required isEqualToString:@"0"]) {
                        self.facebookInstant.hidden = NO;
                    }else{
                        reedemButtonIsactive = YES;
                    }
                    [self.shareButton setBackgroundImage:[UIImage imageNamed:@"facebookCheckHigh"] forState:UIControlStateNormal];

                    self.facebookValue.text = [NSString stringWithFormat:@"$%@",action.offer_value];
                    self.shareButton.hidden = NO;
                    self.shareButton.enabled = NO;
                    self.facebookValue.hidden = NO;
                    
                    [arrayofnumber addObject:@{@"offer_value": [NSNumber numberWithInt:[action.offer_value intValue]],@"id":action.action_id,@"action_type":action.action_type,@"lvl":[NSNumber numberWithInt:3]}];
                    
                    
                }else if ([action.action_type isEqualToString:@"Video"]){
                    if ([action.validation_required isEqualToString:@"0"]) {
                        self.watchInstant.hidden = NO;
                        
                    }else{
                        reedemButtonIsactive = YES;
                    }
                    [self.videoButton setBackgroundImage:[UIImage imageNamed:@"dealVideoCheckHigh"] forState:UIControlStateNormal];

                    self.videoValue.text = [NSString stringWithFormat:@"$%@",action.offer_value];
                    self.videoButton.hidden = NO;
                    self.videoButton.enabled = NO;
                    self.videoValue.hidden =  NO;
                    
                    [arrayofnumber addObject:@{@"offer_value": [NSNumber numberWithInt:[action.offer_value intValue]],@"id":action.action_id,@"action_type":action.action_type,@"lvl":[NSNumber numberWithInt:4]}];
                    
                    
                }else if ([action.action_type isEqualToString:@"Survey"]){
                    if ([action.validation_required isEqualToString:@"0"]) {
                        self.pollInstant.hidden = NO;
                    }else{
                       reedemButtonIsactive = YES;
                    }
                    [self.pollButton setBackgroundImage:[UIImage imageNamed:@"pollCheckHigh"] forState:UIControlStateNormal];
                    self.polValue.text = [NSString stringWithFormat:@"$%@",action.offer_value];
                    self.pollButton.hidden = NO;
                    self.pollButton.enabled = NO;
                    self.polValue.hidden = NO;
                    
                    [arrayofnumber addObject:@{@"offer_value": [NSNumber numberWithInt:[action.offer_value intValue]],@"id":action.action_id,@"action_type":action.action_type,@"lvl":[NSNumber numberWithInt:5]}];
                    
                }
                
            }else{
                
                if ([action.action_type isEqualToString:@"Facebook Share"]) {
                    
                    if ([action.validation_required isEqualToString:@"0"]) {
                        self.facebookInstant.hidden = NO;
                    }
                    
                    self.facebookValue.text = [NSString stringWithFormat:@"$%@",action.offer_value];
                    self.shareButton.hidden = NO;
                    self.shareButton.enabled = YES;
                    self.facebookValue.hidden = NO;
                    
                    [arrayofnumber addObject:@{@"offer_value": [NSNumber numberWithInt:[action.offer_value intValue]],@"id":action.action_id,@"action_type":action.action_type,@"lvl":[NSNumber numberWithInt:3]}];
                    
                    
                }else if ([action.action_type isEqualToString:@"Video"]){
                    if ([action.validation_required isEqualToString:@"0"]) {
                        self.watchInstant.hidden = NO;
                        
                    }
                    self.videoValue.text = [NSString stringWithFormat:@"$%@",action.offer_value];
                    self.videoButton.hidden = NO;
                    self.videoButton.enabled = YES;
                    self.videoValue.hidden =  NO;
                    
                    [arrayofnumber addObject:@{@"offer_value": [NSNumber numberWithInt:[action.offer_value intValue]],@"id":action.action_id,@"action_type":action.action_type,@"lvl":[NSNumber numberWithInt:4]}];
                    
                    
                }else if ([action.action_type isEqualToString:@"Survey"]){
                    if ([action.validation_required isEqualToString:@"0"]) {
                        self.pollInstant.hidden = NO;
                    }
                    self.polValue.text = [NSString stringWithFormat:@"$%@",action.offer_value];
                    self.pollButton.hidden = NO;
                    self.pollButton.enabled = YES;
                    self.polValue.hidden = NO;
                    
                    [arrayofnumber addObject:@{@"offer_value": [NSNumber numberWithInt:[action.offer_value intValue]],@"id":action.action_id,@"action_type":action.action_type,@"lvl":[NSNumber numberWithInt:5]}];
                    
                }
                
            }

            
            
            
        }];
        NSSortDescriptor  *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"offer_value" ascending:NO] ;
        NSSortDescriptor  *lvl = [[NSSortDescriptor alloc] initWithKey:@"lvl" ascending:NO] ;
        NSArray *sortedDeals = [arrayofnumber sortedArrayUsingDescriptors:[NSArray arrayWithObjects:brandDescriptor,lvl, nil]];
        
        
        switch (sortedDeals.count) {
            case 1: //if action is equal to one
            {
                
                NSDictionary *index0 = sortedDeals[0];
                
                
                if([[NSString stringWithFormat:@"%@",index0[@"action_type"]] isEqualToString:@"Video"]){
                    
                    self.watchConstraint.constant = 124;
                    
                }else if ([[NSString stringWithFormat:@"%@",index0[@"action_type"]] isEqualToString:@"Facebook Share"]){
                    self.facebookConstraint.constant = 124;
                }else{
                    self.pollConstraint.constant = 124;
                }
                
                break;
                
            }
                
                
            case 2:
            {
                NSDictionary *index0 = sortedDeals[0];
                NSDictionary *index1 = sortedDeals[1];
                
                
                if([[NSString stringWithFormat:@"%@",index0[@"action_type"]] isEqualToString:@"Video"]){
                    
                    self.watchConstraint.constant = 58;
                    
                }else if ([[NSString stringWithFormat:@"%@",index0[@"action_type"]] isEqualToString:@"Facebook Share"]){
                    self.facebookConstraint.constant = 58;
                }else{
                    self.pollConstraint.constant = 58;
                }
                
                if([[NSString stringWithFormat:@"%@",index1[@"action_type"]] isEqualToString:@"Video"]){
                    
                    self.watchConstraint.constant = 189;
                    
                }else if ([[NSString stringWithFormat:@"%@",index1[@"action_type"]] isEqualToString:@"Facebook Share"]){
                    self.facebookConstraint.constant = 189;
                }else{
                    self.pollConstraint.constant = 189;
                }
                
                
                break;
            }
            case 3:
            {
                NSDictionary *index0 = sortedDeals[0];
                NSDictionary *index1 = sortedDeals[1];
                NSDictionary *index2 = sortedDeals[2];
                
                //arange the Button index 0
                if([[NSString stringWithFormat:@"%@",index0[@"action_type"]] isEqualToString:@"Video"]){
                    
                    self.watchConstraint.constant = 20;
                    
                }else if ([[NSString stringWithFormat:@"%@",index0[@"action_type"]] isEqualToString:@"Facebook Share"]){
                    self.facebookConstraint.constant = 20;
                }else{
                    self.pollConstraint.constant = 20;
                }
                
                if([[NSString stringWithFormat:@"%@",index1[@"action_type"]] isEqualToString:@"Video"]){
                    
                    self.watchConstraint.constant = 124;
                    
                }else if ([[NSString stringWithFormat:@"%@",index1[@"action_type"]] isEqualToString:@"Facebook Share"]){
                    self.facebookConstraint.constant = 124;
                }else{
                    self.pollConstraint.constant = 124;
                }
                
                if([[NSString stringWithFormat:@"%@",index2[@"action_type"]] isEqualToString:@"Video"]){
                    
                    self.watchConstraint.constant = 227;
                    
                }else if ([[NSString stringWithFormat:@"%@",index2[@"action_type"]] isEqualToString:@"Facebook Share"]){
                    self.facebookConstraint.constant = 227;
                }else{
                    self.pollConstraint.constant = 227;
                }
                
                break;
                
            }
                
            default:
            {
                
                self.pollButton.hidden = YES;
                self.videoButton.hidden = YES;
                self.shareButton.hidden = YES;
                self.polValue.hidden = YES;
                self.videoValue.hidden = YES;
                self.facebookValue.hidden = YES;
                
                break;
                
            }
        }
        
    }];
    
}

- (IBAction)shareOnFacebook:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"goToFacebook" sender:self];
}

- (IBAction)viewFactAction:(UIButton *)sender {
}

- (IBAction)shareOnWitterAction:(UIButton *)sender {
}

- (IBAction)scanCheckinAction:(UIButton *)sender {
}

- (IBAction)goBack:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)redeemTapped:(UIButton *)sender {
    
    if (reedemButtonIsactive == NO) {
        [[[UIAlertView alloc]initWithTitle:nil message:@"To redeem offer, please complete an action." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK"  , nil]show];
        return;
    }
    
    
  //  [self performSegueWithIdentifier:@"toPickAStore" sender:self];
    picAstoreVC.fromMenu = YES;
    picAstoreVC.selectedAd = self.selectedAd;
    picAstoreVC.actioncompletionIDMArray = acitonCompletionID;
    
    [self.navigationController pushViewController:picAstoreVC animated:YES];
}

- (IBAction)pollTapped:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"toSurvey" sender:nil];
}

- (IBAction)watchVideo:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"watchVideo" sender:nil];
}

- (IBAction)whereToBuy:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"whereToBuy" sender:nil];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"goToFacebook"]) {
        
        FacebookViewController *facebookViewController = segue.destinationViewController;
        facebookViewController.data = self.selectedAd;
     
        [self.selectedAd.actions enumerateObjectsUsingBlock:^(Action *oneAction, BOOL *stop) {
            
            if ([oneAction.action_type isEqualToString:@"Facebook Share"]) {
                
                facebookViewController.action = oneAction;
                *stop = YES;
            }
        }];
        
    }else if ([segue.identifier isEqualToString:@"toPickAStore"]){
        
        PickAStoreViewController *pickAStoreViewController = segue.destinationViewController;
        
        pickAStoreViewController.fromMenu = YES;
        pickAStoreViewController.actioncompletionIDMArray = acitonCompletionID;
        
    }else if ([segue.identifier isEqualToString:@"toSurvey"]){
        
        SurveyViewController *surveyViewController = segue.destinationViewController;
        
        [self.selectedAd.actions enumerateObjectsUsingBlock:^(Action *oneAction, BOOL *stop) {
            
            if ([oneAction.action_type isEqualToString:@"Survey"]) {
                
                surveyViewController.action = oneAction;
                *stop = YES;
            }
        }];
    }else if([segue.identifier isEqualToString:@"watchVideo"]){
        watchVideoVC = segue.destinationViewController;
        [self.selectedAd.actions enumerateObjectsUsingBlock:^(Action *oneAction, BOOL *stop) {
            
            if ([oneAction.action_type isEqualToString:@"Video"]) {
                
                watchVideoVC.action = oneAction;
                *stop = YES;
            }
        }];
        
    }else if([segue.identifier isEqualToString:@"whereToBuy"]){
        WhereToBuyViewController *wheretoBuyVC = segue.destinationViewController;
        
        wheretoBuyVC.selectedAd = self.selectedAd;
        
    }
}

@end
