//
//  PickAStoreViewController.m
//  BerryCart
//
//  Created by Rommel Bulalacao on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "PickAStoreViewController.h"
#import "HomeCell.h"
#import "SWRevealViewController.h"
#import "ScanReceiptViewController.h"
#import "ScanBarcodeViewController.h"

@interface PickAStoreViewController (){
    
    NSMutableArray *imagesArray;
    UIButton *button;
    NSMutableArray *storeMArray;
    //Flag
    BOOL validationReceipt;
    BOOL validationBarcode;
    BOOL validationReceiptAndBarcode;
    
    //Viewcontroller
    ScanBarcodeViewController *scanBarcodeVC;
    
    
    
}

@end

@implementation PickAStoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    storeMArray = [@[] mutableCopy];
    button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 33, 33)];
    scanBarcodeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ScanBarcodeViewController"];

    
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];


    if (self.fromMenu == NO) {
        
        self.menuButton.hidden = NO;
        // Side Menu
        [button setImage:[UIImage imageNamed:@"menuDefault"] forState:UIControlStateNormal];
        [button addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        self.navigationItem.leftBarButtonItem = barButtonItem;
        
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    }else{
        
        [button removeFromSuperview];
        [self.view removeGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    [self displayStore];

}
-(void)displayStore{
    
    [storeMArray removeAllObjects];
    
    NSArray *fetchedObjects = [UpdateAllData fetchwithEntityName:@"Add_Store_Map" withPredicateKey:@"ad_id" valueToSearch:[NSString stringWithFormat:@"%@",self.selectedAd.ad_id]];
    [fetchedObjects enumerateObjectsUsingBlock:^(Add_Store_Map *storemap, NSUInteger idx, BOOL *stop) {

        Store *thisStore = (Store *)[UpdateAllData fetchwithEntityName:@"Store" withPredicateKey:@"store_id" valueToSearch:[NSString stringWithFormat:@"%@",storemap.store_id]][0];

        Store *parentStore = (Store *) [UpdateAllData fetchwithEntityName:@"Store" withPredicateKey:@"parent_store_id" valueToSearch:[NSString stringWithFormat:@"%@",thisStore.parent_store_id]][0];
        NSLog(@"parentStore %@",parentStore);

        ![storeMArray containsObject:parentStore] ? [storeMArray addObject:parentStore] : nil;
        
        (fetchedObjects.count-1) == idx ?[self.tableView reloadData]:nil;
        
    }];
    

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return storeMArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Store *selectedStore = storeMArray[indexPath.row];
    
    static NSString *CellIdentifier = @"HomeCell";
    HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.pickastoreName.text = selectedStore.store_name;

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    validationReceipt = NO;
    validationBarcode = NO;
    validationReceiptAndBarcode = NO;
    
    
    if (self.selectedAd) {
        
        [BerryCart_API checkactionByUserID:theAppDelegate.loggedInUser[@"data"][@"Debug"][@"CustomerId"] completaion:^(NSDictionary *dict) {

            [self.selectedAd.actions enumerateObjectsUsingBlock:^(Action *oneAction, BOOL *stop) {
                
                __block BOOL isCompleted = NO;
                
                for (NSDictionary *oneDict in (NSArray *)dict[@"data"]) {
                    if ([oneAction.action_id isEqualToNumber:[NSNumber numberWithInt:[oneDict[@"action_id"]intValue]]]) {
                        isCompleted = YES;
                        break;
                    }
                }
                
                if (isCompleted) {
                    
                    if (![oneAction.validation_required isEqualToString:@"0"]) {
                        if ([oneAction.validation_required isEqualToString:@"1"]) {
                            validationReceipt = YES;
                        }else if ([oneAction.validation_required isEqualToString:@"2"]){
                            validationBarcode = YES;
                        }else if ([oneAction.validation_required isEqualToString:@"3"]){
                            validationReceiptAndBarcode = YES;
                            *stop = YES;
                        }
                    }
                }
            }];
            
            if (((validationReceipt == YES && validationBarcode == YES) || (validationReceipt == YES && validationReceiptAndBarcode == YES)) || (validationReceipt == YES || validationReceiptAndBarcode == YES)) {
                [self performSegueWithIdentifier:@"toScanReceipt" sender:nil];
            }else{
                [self.navigationController pushViewController:scanBarcodeVC animated:YES];
            }
            
        }];
        
    }
    

    

    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"toScanReceipt"]) {
        ScanReceiptViewController *scanReceipt = segue.destinationViewController;
        scanReceipt.validationBarcode = validationBarcode;
        scanReceipt.validationReceiptAndBarcode = validationReceiptAndBarcode;
        scanReceipt.selectedAd = self.selectedAd;
        scanReceipt.completedActionMArray = self.actioncompletionIDMArray;
        
    }
    
}



@end
