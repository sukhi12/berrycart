//
//  FacebookViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "FacebookViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "BerryCart_API.h"
#import "PopUpViewController.h"


@interface FacebookViewController (){
    PopUpViewController *popupvc;
}

@end

@implementation FacebookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    popupvc = [[PopUpViewController alloc]init];
    popupvc.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareLinkWithAPICalls) name:@"HasSession" object:nil];
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadData{
    
    NSLog(@"NSDATA %@",self.data);
    
    self.priceToBeEarned.text = [NSString stringWithFormat:@"$%@",self.action.offer_value];
    self.productImageView.image = [UIImage imageNamed:self.data.image];
    self.productNameLabel.text = self.data.title;
    self.descriptionLabel.text = [NSString stringWithFormat:@"%@\n%@", self.data.line1, self.data.line2];
    
}

- (IBAction)goBack:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)postToFacebook:(UIButton *)sender {
    
    NSLog(@"POST TO FACEBOOK");
    
    // Check if the Facebook app is installed and we can present the share dialog
    FBShareDialogParams *params = [[FBShareDialogParams alloc] init];
    params.link = [NSURL URLWithString:@"http://www.itunes.apple.com/berrycart_test"];
    params.name = self.data.title;
    params.caption = self.data.line1;
    params.picture = [NSURL URLWithString:@"http://i.imgur.com/g3Qc1HN.png"];
    params.description = self.data.line2;
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        // Present the share dialog
        [FBDialogs presentShareDialogWithLink:params.link
                                         name:params.name
                                      caption:params.caption
                                  description:params.description
                                      picture:params.picture
                                  clientState:nil
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              //NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
                                              NSLog(@"Error publishing story :%@", error.localizedDescription);
                                          } else {
                                              [BerryCart_API completeAction:@{@"action_id": [NSString stringWithFormat:@"%@",self.action.action_id], @"user_id": theAppDelegate.loggedInUser[@"data"][@"Debug"][@"CustomerId"]} completion:^(NSDictionary *dict) {
                                                  
                                                  [popupvc callCompletedWithRootView:self.view automaticClose:YES withDelay:2.0];
                                                  
                                              }];
                                              
                                              

                                          }
                                      }];

    } else {
        // Present the feed dialog
        
        [self shareLinkWithAPICalls];
        
    }

}

- (void)shareLinkWithAPICalls{
    
    // Put together the dialog parameters
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [NSString stringWithFormat:@"%@",self.data.title], @"name",
                                   @"Build great social apps and get more installs.", @"caption",
                                   [NSString stringWithFormat:@"%@\n%@", self.data.line1, self.data.line2], @"description",
                                   @"https://developers.facebook.com/docs/ios/share/", @"link",
                                   @"http://i.imgur.com/g3Qc1HN.png", @"picture",
                                   nil];
    
    // Show the feed dialog
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                  if (error) {
                                                      // An error occurred, we need to handle the error
                                                      // See: https://developers.facebook.com/docs/ios/errors
                                                      
                                                      NSLog(@"ERROR :%@", error.localizedDescription);
                                                      
                                                  } else {
                                                      if (result == FBWebDialogResultDialogNotCompleted) {
                                                          // User cancelled.
                                                          NSLog(@"User cancelled.");
                                                      } else {
                                                          // Handle the publish feed callback
                                                          NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                          
                                                          if (![urlParams valueForKey:@"post_id"]) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                              
                                                          } else {
                                                              
                                                              [BerryCart_API completeAction:@{@"action_id": [NSString stringWithFormat:@"%@",self.action.action_id], @"user_id": theAppDelegate.loggedInUser[@"data"][@"Debug"][@"CustomerId"]} completion:^(NSDictionary *dict) {
                                                                  
                                                                  [popupvc callCompletedWithRootView:self.view automaticClose:YES withDelay:2.0];
                                                                  
                                                              }];
                                                              // User clicked the Share button
                                                              NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                              NSLog(@"result %@", result);
                                                          }
                                                      }
                                                  }
                                              }];
    
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

- (void)makeRequestToShareLink {
    
    // NOTE: pre-filling fields associated with Facebook posts,
    // unless the user manually generated the content earlier in the workflow of your app,
    // can be against the Platform policies: https://developers.facebook.com/policy
    
    // Put together the dialog parameters
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"Sharing Tutorial", @"name",
                                   @"Build great social apps and get more installs.", @"caption",
                                   @"Allow your users to share stories on Facebook from your app using the iOS SDK.", @"description",
                                   @"https://developers.facebook.com/docs/ios/share/", @"link",
                                   @"http://www.berrycart.com/wp-content/uploads/2012/08/berrycart-logo2.png", @"picture",
                                   nil];
    
    // Make the request
    [FBRequestConnection startWithGraphPath:@"/me/feed"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  // Link posted successfully to Facebook

                              } else {
                                  // An error occurred, we need to handle the error
                                  // See: https://developers.facebook.com/docs/ios/errors

                              }
                          }];
}

-(void)PopupDidclose{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
