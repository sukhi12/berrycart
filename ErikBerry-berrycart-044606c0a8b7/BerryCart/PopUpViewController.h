//
//  PopUpViewController.h
//  BerryCart
//
//  Created by kristopher Amiel Reyes on 2/7/14.
//  Copyright (c) 2014 Codemagnus. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PopUpDelegate;
@interface PopUpViewController : UIViewController{
   
}
@property (strong, nonatomic) id <PopUpDelegate> delegate;

@property (nonatomic,strong) UIView *darkView;
@property (nonatomic) BOOL animationIndicator;

-(void)callCompletedWithRootView:(UIView *)view automaticClose:(BOOL)isautomatic withDelay:(float)interval;
-(void)CallPopupWithImage:(UIImage *)imagename withView:(UIView *)view automaticClose:(BOOL)isautomatic withDelay:(float)interval withHeight:(float)height withwidth:(float)width;
-(void)ExitAnimationWithView;
@end
@protocol PopUpDelegate <NSObject>
@required
@optional

-(void)PopupDidclose;

@end