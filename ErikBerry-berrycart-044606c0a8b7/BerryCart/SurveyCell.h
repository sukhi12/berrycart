//
//  SurveyCell.h
//  BerryCart
//
//  Created by Romeo Flauta on 2/3/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SurveyCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *buttonImageView;
@property (nonatomic, weak) IBOutlet UILabel *answerLabel;

@end
