//
//  PickAStoreViewController.h
//  BerryCart
//
//  Created by Rommel Bulalacao on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Store.h"
#import "Ad.h"
#import "Add_Store_Map.h"
#import "UpdateAllData.h"
#import "BerryCart_API.h"
#import "Action.h"


@interface PickAStoreViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>


- (IBAction)goBack:(UIButton *)sender;
@property (strong, nonatomic) Ad *selectedAd;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) BOOL fromMenu;
@property (nonatomic,strong)NSMutableArray *actioncompletionIDMArray;


@end
