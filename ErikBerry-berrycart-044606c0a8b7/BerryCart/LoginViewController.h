//
//  LoginViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailAddressOutlet;
@property (weak, nonatomic) IBOutlet UITextField *passwordOutlet;


@property (weak, nonatomic) IBOutlet UIView *view1;
- (IBAction)loginTapped:(UIButton *)sender;
- (IBAction)logInToFacebook:(UIButton *)sender;

@end
