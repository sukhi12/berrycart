//
//  TransferToPayPalViewController.h
//  BerryCart
//
//  Created by Rommel Bulalacao on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransferToPayPalViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) NSString *emailAddress;
@property (weak, nonatomic) IBOutlet UILabel *emailAddressLabel;
@property (weak, nonatomic) IBOutlet UIView *textfieldView;
@property (weak, nonatomic) IBOutlet UITextField *amountTextfield;

- (IBAction)goBack:(UIButton *)sender;
- (IBAction)transferFundsToPaypal:(UIButton *)sender;

@end
