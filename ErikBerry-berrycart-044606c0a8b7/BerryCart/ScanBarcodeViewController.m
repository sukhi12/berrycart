//
//  ScanBarcodeViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "ScanBarcodeViewController.h"
#import "BerryCart_CoreData.h"
#import "BarCode.h"
#import "Ad.h"
#import "BerryCart_API.h"
#import "MBProgressHUD.h"

@interface ScanBarcodeViewController (){
    
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;
    
    UIView *_highlightView;

}

@end

@implementation ScanBarcodeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self addScanner];
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegate and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = self.selectedAd.title;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //[self performSegueWithIdentifier:@"toScanReceipt" sender:nil];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

#pragma mark - Scan Delegate Method

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    
    CGRect highlightViewRect = CGRectZero;
    AVMetadataMachineReadableCodeObject *barCodeObject;
    NSString *detectionString = nil;
    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
    
    for (AVMetadataObject *metadata in metadataObjects) {
        
        for (NSString *type in barCodeTypes) {
            
            if ([metadata.type isEqualToString:type])
            {
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                highlightViewRect = barCodeObject.bounds;
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                
                [_session stopRunning];
               
                if (detectionString) {
                 
                    [self findThisProductData:detectionString];
                    
                }
                
                break;
            }
        }
        break;
    }
    
}

- (void) findThisProductData: (NSString *) detectionString{
    self.statusBar.textColor = [UIColor whiteColor];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    [BerryCart_API getBarCodes:^(NSDictionary *resul) {
        __block  BOOL isMATCHED = NO;
        NSMutableArray *barcodes = [@[]mutableCopy];
        [(NSArray *)resul[@"data"] enumerateObjectsUsingBlock:^(NSDictionary  *obj, NSUInteger idx, BOOL *stop) {
            

            
            
            if ([[NSString stringWithFormat:@"%@",obj[@"barcode"]] isEqualToString:detectionString]) {
                self.statusBar.text = @"PRODUCT MATCHED!";
                [barcodes addObject:detectionString];
                isMATCHED = YES;
                *stop = YES;
            }
            
        }];
        
        if (isMATCHED == NO) {
            self.statusBar.textColor = [UIColor redColor];
            self.statusBar.text = @"Product does not match any with pending points.";

        }else{
            
            NSMutableString *ids = [NSMutableString string];
            
            [self.actionCompletionID enumerateObjectsUsingBlock:^(NSString *oneAction, NSUInteger idx, BOOL *stop) {
                if(ids.length>0){
                    [ids appendString:[NSString stringWithFormat:@",%@",oneAction]];
                }else{
                    [ids appendString:[NSString stringWithFormat:@"%@",oneAction]];
                }
            }];
            
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObject:ids forKey:@"action_completion_ids"];
            int index = 0;
            for (NSString *barcode in barcodes) {
                [params setObject:barcode forKey:[NSString stringWithFormat:@"barcode[%i]",index]];
                index ++;
            }
            
            
            
            
            [BerryCart_API sendActionProofs:params withArrayOfImage:self.receiptImageArray withIdentifier:@"reciept" completion:^(NSDictionary *dict) {
                [MBProgressHUD hideHUD];

                [self.navigationController popToViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count -4] animated:YES];
                
            }];
          
        }
        
        
        
    }];
    
    
    
    
    
    /*
    [BerryCart_CoreData fetchObjectsWithPredicate:@"BarCode" andPredicateString:barcodePredicate completion:^(NSArray *resul) {
       
        NSLog(@"RESULT :%@", resul);
        
    }];
     */

    /*
    [BerryCart_CoreData fetchObjectsWithoutPredicate:@"BarCode" completion:^(NSArray *resul) {
        
        for (BarCode *oneBarcode in resul) {
            NSLog(@"Barcode :%@", oneBarcode);
        }
        
    }];
     */
  /*
    [BerryCart_CoreData fetchObjectsWithPredicate:@"BarCode" andPredicateString:@"SELF.barcode = %@" andPredicateArgument:detectionString completion:^(NSArray *result) {
        
        if (result.count > 0) {
            
            BarCode *thisBarcode = result[0];

            [BerryCart_CoreData fetchObjectsWithPredicate:@"Ad" andPredicateString:@"SELF.ad_id = %@" andPredicateArgument:[NSString stringWithFormat:@"%i", [thisBarcode.ad_id intValue]] completion:^(NSArray *result) {
                
                for (Ad *oneAd in result) {
                    
                    NSLog(@"oneAD.title :%@", oneAd.title);
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"AD TITLE" message:[NSString stringWithFormat:@"%@", oneAd.title] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    
                    [alertView show];
                    
                }
                
            }];
        }
        
        
    }];*/
}

- (void) addScanner{
    
    /*
     _highlightView = [[UIView alloc] init];
     _highlightView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
     _highlightView.layer.borderColor = [UIColor greenColor].CGColor;
     _highlightView.layer.borderWidth = 3;
     [self.scannerView addSubview:_highlightView];
     */
    
    _session = [[AVCaptureSession alloc] init];
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    
    if (_input) {
        [_session addInput:_input];
    } else {
        
        NSLog(@"Error: %@", error);
    }
    
    _output = [[AVCaptureMetadataOutput alloc] init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session addOutput:_output];
    
    _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
    
    _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    _prevLayer.frame = self.scannerView.bounds;
    _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.scannerView.layer addSublayer:_prevLayer];
    [_session startRunning];
    
    [self.scannerView bringSubviewToFront:self.styleImageView];
    
    //[self.scannerView bringSubviewToFront:_highlightView];
    
}

- (IBAction)goBack:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
