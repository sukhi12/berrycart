//
//  SettingsViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/27/14.
//  Copyright (c) 2014 BerryCart.. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
- (IBAction)linktoFacebookAccount:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *facebookLabel;

-(IBAction)logoutClicked;

@end
