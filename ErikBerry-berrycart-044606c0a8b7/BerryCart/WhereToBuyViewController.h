//
//  WhereToBuyViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Ad.h"
#import "Add_Store_Map.h"
#import "AppDelegate.h"
#import "UpdateAllData.h"
#import "Store.h"
#import "wheretobuyCell.h"
@import CoreLocation;

@interface WhereToBuyViewController : UIViewController<MKAnnotation,MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) Ad *selectedAd;
@property (weak, nonatomic) IBOutlet MKMapView *mapKit;
@property (weak, nonatomic) IBOutlet UITableView *storeTableView;

- (IBAction)listAndMapButton:(UIButton *)sender;
- (IBAction)segmentedTapped:(UIButton *)sender;
- (IBAction)goBack:(UIButton *)sender;

@end
