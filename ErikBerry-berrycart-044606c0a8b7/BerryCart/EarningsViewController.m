//
//  EarningsViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "EarningsViewController.h"
#import "EarningsCell.h"
#import "SWRevealViewController.h"
#import "PickAStoreViewController.h"
#import "PaypalViewController.h"
#import "BerryCart_API.h"

@interface EarningsViewController (){
    
    NSMutableArray *historyArray;
}

@end

@implementation EarningsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    historyArray = [NSMutableArray arrayWithObjects:@"$.55 earned for Sun Organic Chips", @"$.55 earned for Sun Organic Chips", @"$.55 earned for Sun Organic Chips", @"$.55 earned for Sun Organic Chips", @"$.55 earned for Sun Organic Chips", @"$.55 earned for Sun Organic Chips", nil];
    
    [self loadScrollView];

    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];

}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];;
    self.navigationController.navigationBarHidden = NO;
    
    
    [BerryCart_API GetPendingAndEarnedcompletion:^(NSDictionary *resul) {

        self.earnedLabel.text = [NSString stringWithFormat:@"$%0.2f",[resul[@"data"][@"EarnedCash"] floatValue]];
        self.pendingLabel.text = [NSString stringWithFormat:@"$%0.2f",[resul[@"data"][@"PendingCash"] floatValue]];
        
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadScrollView{
    
    UIView *view1 = [[NSBundle mainBundle] loadNibNamed:@"EarningsView" owner:self options:nil][0];
    
    for (UIView *view in view1.subviews) {
        
        if (![view isKindOfClass:[UIButton class]] && ![view isKindOfClass:[UILabel class]]) {
            
            view.layer.cornerRadius = 4;
            view.layer.masksToBounds = YES;
            view.layer.shadowColor = [UIColor grayColor].CGColor;
            view.layer.borderWidth = .5;
            
        }
        
    }

    [self.verifyPurchase addTarget:self action:@selector(buttonsTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.earningScrollView addSubview:view1];
    
    [view1 bringSubviewToFront:self.view3];
    [view1 bringSubviewToFront:self.view1];
    [view1 bringSubviewToFront:self.view2];

    self.tableView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 50 * historyArray.count);
    
    self.view3.frame = CGRectMake(self.view3.frame.origin.x, self.view3.frame.origin.y, self.view3.frame.size.width, 50 * historyArray.count);
    
    view1.frame = CGRectMake(0, 0, self.view.frame.size.width, 350 + self.view3.frame.size.height + 15);

    [self.earningScrollView setContentSize:CGSizeMake(view1.frame.size.width, view1.frame.size.height)];
    
}

- (void) fetchUserData{
    
    [BerryCart_API getUserData:nil :^(NSDictionary *resul) {
       
        NSLog(@"result :%@", resul);
        
        self.earnedLabel.text = [NSString stringWithFormat:@"$%@",resul[@"data"][@"EarnedCash"]];
                                 
        self.pendingLabel.text = [NSString stringWithFormat:@"$%@",resul[@"data"][@"PendingCash"]];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return historyArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    EarningsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EarningsCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EarningsCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    return cell;
}

- (IBAction)goBack:(UIButton *)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (IBAction)buttonsTapped:(UIButton *)sender {
    
    if (sender.tag == 0) { // Transfer to paypal
        
        PaypalViewController *payPalViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PaypalViewController"];
        
        [self.navigationController pushViewController:payPalViewController animated:YES];
        
    }else if (sender.tag == 1){ // Verify Purchase
        
        PickAStoreViewController *pickAStoreViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickAStoreViewController"];
        pickAStoreViewController.fromMenu = YES;
        [self.navigationController pushViewController:pickAStoreViewController animated:YES];
    }
}
@end
