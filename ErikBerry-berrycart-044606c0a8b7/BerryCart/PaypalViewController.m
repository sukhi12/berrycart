//
//  PaypalViewController.m
//  BerryCart
//
//  Created by Rommel Bulalacao on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "PaypalViewController.h"
#import "TransferToPayPalViewController.h"

@interface PaypalViewController ()

@end

@implementation PaypalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view1.layer.cornerRadius = 4;
    self.view1.layer.masksToBounds = YES;
    self.view1.layer.shadowColor = [UIColor grayColor].CGColor;
    self.view1.layer.borderWidth = .5;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)goBack:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submitTapped:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"toTransferPaypal" sender:self];
}

- (IBAction)createAccountAtPayPal:(UIButton *)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.paypal.com"]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"toTransferPaypal"]) {
        
        TransferToPayPalViewController *transferToPayPalViewController = segue.destinationViewController;
        
        transferToPayPalViewController.emailAddress = self.paypalEmailAddressTextField.text;
        
    }
}

@end
