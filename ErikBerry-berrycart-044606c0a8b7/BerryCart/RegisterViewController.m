//
//  LoginViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "RegisterViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"

#import "BerryCart_API.h"
#import <UIKit/UIKit.h>


@interface RegisterViewController (){
    SWRevealViewController *SWRVC;

}

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    SWRVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)backButton:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextTapped:(UIButton *)sender {
    
    NSString *emailString = [self.emailAddressTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *passwordString = [self.passwordTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *reenterPasswordString = [self.reenterPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];


    if (emailString.length > 0 && passwordString.length > 0 && reenterPasswordString.length > 0) {
        
        if (![self NSStringIsValidEmail:emailString]) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid Email Address" message:@"Please enter a valid email address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alertView show];
            
            return;
        }
        
        if (![passwordString isEqualToString:reenterPasswordString]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Passwords don't match" message:@"Please create/re-enter a password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alertView show];
            
            return;

        }
        
        if (!(passwordString.length > 5)) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid Password" message:@"Your password must contain at least 6 characters." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alertView show];
            
            return;
            
        }
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        
        [BerryCart_API registerDeviceAction:^(BOOL isRegistered) {
            
            if (isRegistered) {
                if (![BerryCart_API user_session]){
                    //create sesion
                    [BerryCart_API createSessionAction:^(BOOL hasSession) {
                        if (hasSession) {
                            [self registerNewUser];
                        }
                    }];
                }else{
                    [self registerNewUser];
                }
            }
        }];
        
        
        
    }else{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Required Fields" message:@"All fields are required." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alertView show];
    }

    
    
 //   [self performSegueWithIdentifier:@"toMainView" sender:nil];
    
}



-(void)registerNewUser{
    
    
    
    if ([BerryCart_API facebookIDisNotregistered] == YES) {
        
        [BerryCart_API createNewUser:[NSDictionary dictionaryWithObjects:@[[BerryCart_API user_session],self.emailAddressTextfield.text,self.passwordTextfield.text,[BerryCart_API facebookID]] forKeys:@[@"session",@"email",@"pin",@"facebook_id"]] :^(NSDictionary *resul) {
            
            if([[NSString stringWithFormat:@"%@",resul[@"message"]] isEqualToString:@"success"]){
                [BerryCart_API loginAction:[NSDictionary dictionaryWithObjects:@[[BerryCart_API user_session],self.emailAddressTextfield.text,self.passwordTextfield.text,@"login"] forKeys:@[@"session",@"email",@"pin",@"action"]] completion:^(NSDictionary *resul) {
                    
                    [MBProgressHUD hideHUD];
                    
                    [theAppDelegate.loggedInUser setValue:resul[@"data"][@"email"] forKey:@"email"];
                    
                    if ([[NSString stringWithFormat:@"%@",resul[@"message"]] isEqualToString:@"success"]) {
                        
                        UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"Success: Account Created!" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [successAlert show];
                        
                        [self performSelector:@selector(dismissAlertView:) withObject:successAlert afterDelay:2];
                        
                    }else if ([[NSString stringWithFormat:@"%@",resul[@"messege"]] isEqualToString:@""]){
                        
                    }else{
                        
                    }
                    
                }];
            }else{
                
                NSLog(@"result of unsuccessful registration: %@", resul);
                if ([resul[@"message"] isEqualToString:@"email already exists"]) {
                    
                    UIAlertView *existsAlert = [[UIAlertView alloc]initWithTitle:@"Duplicate Accounts" message:@"Account already exists." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [existsAlert show];
                }
            }
        }];
        
    }else{
        
        [BerryCart_API createNewUser:[NSDictionary dictionaryWithObjects:@[[BerryCart_API user_session],self.emailAddressTextfield.text,self.passwordTextfield.text] forKeys:@[@"session",@"email",@"pin"]] :^(NSDictionary *resul) {
            
            if([[NSString stringWithFormat:@"%@",resul[@"message"]] isEqualToString:@"success"]){
                [BerryCart_API loginAction:[NSDictionary dictionaryWithObjects:@[[BerryCart_API user_session],self.emailAddressTextfield.text,self.passwordTextfield.text,@"login"] forKeys:@[@"session",@"email",@"pin",@"action"]] completion:^(NSDictionary *resul) {
                    [MBProgressHUD hideHUD];

                    [theAppDelegate.loggedInUser setValue:resul[@"data"][@"email"] forKey:@"email"];
                    
                    if ([[NSString stringWithFormat:@"%@",resul[@"message"]] isEqualToString:@"success"]) {
                        
                        UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"Success: Account Created!" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [successAlert show];
                        
                        [self performSelector:@selector(dismissAlertView:) withObject:successAlert afterDelay:2];
                        
                        
                    }else if ([[NSString stringWithFormat:@"%@",resul[@"messege"]] isEqualToString:@""]){
                        
                    }else{
                        
                    }
                    
                }];
                
            }else{
                
                NSLog(@"result of unsuccessful registration: %@", resul);
                
                if ([resul[@"message"] isEqualToString:@"email already exists"]) {
                    
                    UIAlertView *existsAlert = [[UIAlertView alloc]initWithTitle:@"Duplicate Accounts" message:@"Account already exists." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [existsAlert show];
                }
            }
            
            
        }];
    }
    
    if (self.faceBookId) {
        
        
        
        
    }else{
        
       
    }
}


- (IBAction)goBack:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)toPrivacyLink:(UIButton *)sender
{
    if(sender.tag==1)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.berrycart.com/terms"]];
    }
    else if(sender.tag==2)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.berrycart.com/privacy"]];
    }


}

-(void)dismissAlertView:(UIAlertView *) alertView{
    
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
    
    [self presentViewController:SWRVC animated:YES completion:nil];

}

@end
