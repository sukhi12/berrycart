//
//  SurveyViewController.m
//  BerryCart
//
//  Created by Rommel Bulalacao on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "SurveyViewController.h"
#import "Action_Rule.h"
#import "BerryCart_API.h"
#import "Action_Survey_Answer.h"
#import "AppDelegate.h"
#import "SurveyCell.h"
#import "MBProgressHUD.h"
#import "PopUpViewController.h"

@interface SurveyViewController (){
    
    NSMutableArray *answerMarray;
    Action_Rule *selectedActionRule;
    PopUpViewController *popupvc;
    int answer_counter;
}

@end

@implementation SurveyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    popupvc = [[ PopUpViewController alloc]init];
    popupvc.delegate = self;
    answerMarray = [NSMutableArray array];
    //set array that displays whether answer has been selected or not
    self.answerTable.layer.cornerRadius = 4;
    self.answerTable.layer.masksToBounds = YES;
    self.answerTable.layer.shadowColor = [UIColor grayColor].CGColor;
    self.answerTable.layer.borderWidth = .5;

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [answerMarray removeAllObjects];
    [self.answerTable reloadData];
    answer_counter = 0;
    self.earnLabel.text = [NSString stringWithFormat:@"$%@",self.action.offer_value];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Action_Rule" inManagedObjectContext:theAppDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"action_id = %i", [self.action.action_id intValue]];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [theAppDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    selectedActionRule = fetchedObjects[0];
    self.questionTextview.editable = YES;
    self.questionTextview.text = selectedActionRule.question;
    self.questionTextview.editable = NO;

    //survey_type = 0  single answer
    //shuffle_plug = 0 no shuffle
    
    NSSortDescriptor *sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"action_survey_answer_id" ascending: YES];
    
        if ([selectedActionRule.shuffle_flag isEqualToNumber:[NSNumber numberWithInt:1]]) {//shuffle 1

            for (Action_Survey_Answer *oneAction in selectedActionRule.action_survey_answers) {

                [answerMarray addObject:@{@"answer" : oneAction.answer,@"isSelected": @"0"}];
                
            }
            
        }else{// shuffle 0
            
            for (Action_Survey_Answer *oneAction in [[selectedActionRule.action_survey_answers allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortOrder]]) {
                [answerMarray addObject:@{@"answer" : oneAction.answer,@"isSelected": @"0"}];
            }
        }

    
    
    CGSize rect = [self.questionTextview sizeThatFits:CGSizeMake(self.questionTextview.frame.size.width, MAXFLOAT)];

    
    if (rect.height <44) {
        rect.height = 44;
        self.questionHeight.constant = 44;
        //self.questionTextview.frame   = rect;
    }else if(rect.height >=44 && rect.height <=99){
        self.questionHeight.constant = rect.height;
        //self.questionTextview.frame   = rect;
    }else{
        rect.height = 99;
        self.questionHeight.constant = 99;
        //self.questionTextview.frame   = rect;
    }

    self.tableHeight.constant = answerMarray.count * 44;
    
    [self.answerTable reloadData];
    

}



#pragma mark Table delegate

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return answerMarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SurveyCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSDictionary *oneDict = answerMarray[indexPath.row];
    

    
    cell.answerLabel.text = oneDict[@"answer"];
    if ([selectedActionRule.survey_type isEqualToNumber:[NSNumber numberWithInt:0]]) {// single answer
    
        if ([[NSString stringWithFormat:@"%@",oneDict[@"isSelected"]] isEqualToString:@"0"]) {
            cell.buttonImageView.image = [UIImage imageNamed:@"circle_unselected.png"];
        }else{
            cell.buttonImageView.image = [UIImage imageNamed:@"circle_selected.png"];
            
        }
        
    }else{

        
        if ([[NSString stringWithFormat:@"%@",oneDict[@"isSelected"]] isEqualToString:@"0"]) {
            cell.buttonImageView.image = [UIImage imageNamed:@"square_unselected.png"];
        }else{
            cell.buttonImageView.image = [UIImage imageNamed:@"square_selected.png"];
        }
    }
    
    

    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSMutableDictionary *oneDict = [answerMarray[indexPath.row] mutableCopy];

    SurveyCell *surveyCell = (SurveyCell *)[tableView cellForRowAtIndexPath:indexPath];
    surveyCell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIImageView *imageView = surveyCell.buttonImageView;
   
    
    
    if ([[NSString stringWithFormat:@"%@",oneDict[@"isSelected"]] isEqualToString:@"0"]) {
        if ([selectedActionRule.survey_type isEqualToNumber:[NSNumber numberWithInt:0]]) {// single answer
            if (answer_counter >=1) {
                return;
            }
            answer_counter +=1;
            [oneDict setObject:@"1" forKey:@"isSelected"];
            imageView.image = [UIImage imageNamed:@"circle_selected.png"];
  
            
        }else{
            answer_counter +=1;
            [oneDict setObject:@"1" forKey:@"isSelected"];
            imageView.image = [UIImage imageNamed:@"square_selected.png"];
        }
     
    }else{
        if ([selectedActionRule.survey_type isEqualToNumber:[NSNumber numberWithInt:0]]) {// single answer
            
            answer_counter -=1;
            [oneDict setObject:@"0" forKey:@"isSelected"];
            imageView.image = [UIImage imageNamed:@"circle_unselected.png"];
        }else{
            answer_counter -=1;
            [oneDict setObject:@"0" forKey:@"isSelected"];
            imageView.image = [UIImage imageNamed:@"square_unselected.png"];
        }
      
    }

    
    [answerMarray replaceObjectAtIndex:indexPath.row withObject:[oneDict copy]];
    
}



-(Action_Survey_Answer *)getOneActioneAnswerFromShuffle:(Action_Rule *)rule{
    
    int index = arc4random() %(rule.action_survey_answers.count)-1;
    
    return [rule.action_survey_answers allObjects][index];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submitAction:(UIButton *)sender {
    
    [popupvc CallPopupWithImage:[UIImage imageNamed:@"your image"] withView:self.view automaticClose:YES withDelay:3.0 withHeight:315 withwidth:315];
    
    if ([answerMarray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isSelected == %@",@"1"]].count >0) {
        sender.enabled = NO;
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        [BerryCart_API completeAction:@{@"action_id": [NSString stringWithFormat:@"%@",self.action.action_id], @"user_id": theAppDelegate.loggedInUser[@"data"][@"Debug"][@"CustomerId"]} completion:^(NSDictionary *dict) {
            sender.enabled = YES;
            [popupvc callCompletedWithRootView:self.view automaticClose:YES withDelay:2.0];
            

            
        }];
    }
    

}

-(void)PopupDidclose{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
