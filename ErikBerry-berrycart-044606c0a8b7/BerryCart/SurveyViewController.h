//
//  SurveyViewController.h
//  BerryCart
//
//  Created by Rommel Bulalacao on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Action.h"
#import "PopUpViewController.h"


@interface SurveyViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,PopUpDelegate>
- (IBAction)goBack:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UILabel *earnLabel;
@property (nonatomic, strong) Action *action;
@property (weak, nonatomic) IBOutlet UITextView *questionTextview;
@property (weak, nonatomic) IBOutlet UITableView *answerTable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *questionHeight;
- (IBAction)submitAction:(UIButton *)sender;




@end
