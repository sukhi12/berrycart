//
//  WatchVideoViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "WatchVideoViewController.h"
#import "Action_Rule.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIImageView+AFNetworking.h"
#import "HCYoutubeParser.h"
#import "BerryCart_API.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"


@interface WatchVideoViewController (){
     MPMoviePlayerController *mc;
     BOOL userPressNext;
    BOOL isCompleted;
    PopUpViewController *popupvc;
}

@end

@implementation WatchVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    popupvc = [[PopUpViewController alloc]init];
    popupvc.delegate = self;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myMovieFinished:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayerViewController.moviePlayer];
    
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(playBackStateChanged:)
                                                 name: MPMoviePlayerPlaybackStateDidChangeNotification
                                               object: moviePlayerViewController.moviePlayer];
    
}

- (void) viewWillAppear:(BOOL)animated{
    userPressNext = NO;
    
    if (isCompleted == NO) {
        self.submitButton.hidden = YES;
    }else{
        self.submitButton.hidden = NO;
    }
    
    
    [super viewWillAppear:YES];
    self.earnValue.text = [NSString stringWithFormat:@"$%@",self.action.offer_value];

    [self.youtubeThumbNail setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",[self getyoutubeID]]]];
}

-(NSString *)getyoutubeID{
  NSArray *youtubeID = [self.action.action_rule.video_url componentsSeparatedByString:@"watch?v="];
    
    return (NSString *)youtubeID[1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(UIButton *)sender {
    isCompleted = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)playButton:(UIButton *)sender {
    
    
    NSDictionary *videos = [HCYoutubeParser h264videosWithYoutubeURL:[NSURL URLWithString:self.action.action_rule.video_url]];
    
    moviePlayerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:[videos objectForKey:@"medium"]]];
   // moviePlayerViewController.moviePlayer.controlStyle = MPMovieControlModeVolumeOnly;
    [moviePlayerViewController.moviePlayer prepareToPlay];
    moviePlayerViewController.view.frame = CGRectMake(0, -50, 320, self.view.frame.size.height + 50);
    [moviePlayerViewController.moviePlayer play];
    
    moviePlayerViewController.view.userInteractionEnabled = YES;

    [self presentMoviePlayerViewControllerAnimated:moviePlayerViewController];
    
    
}

- (IBAction)submitButton:(UIButton *)sender {
    sender.enabled = NO;
    
    [self sendCompletedAction:sender];

}

-(void)sendCompletedAction:(UIButton *)sender {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [BerryCart_API completeAction:@{@"action_id": [NSString stringWithFormat:@"%@",self.action.action_id], @"user_id": theAppDelegate.loggedInUser[@"data"][@"Debug"][@"CustomerId"]} completion:^(NSDictionary *dict) {
        sender.enabled = YES;
        [popupvc callCompletedWithRootView:self.view automaticClose:YES withDelay:2.0];
        
        [MBProgressHUD hideHUD];
    }];


}


-(void)myMovieFinished:(NSNotification*)aNotification
{
    NSDictionary *notificationUserInfo = [aNotification userInfo];
    NSNumber *resultValue = [notificationUserInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    MPMovieFinishReason reason = [resultValue intValue];
    //success = 0
    if (userPressNext == NO) {
        if ([[NSString stringWithFormat:@"%ld",(long)reason] isEqualToString:@"0"]) {
            [self sendCompletedAction:nil];
           // isCompleted = YES;
            
        }else{
            MPMoviePlayerController *moviePlayer = [aNotification object];
            [moviePlayer setFullscreen:NO animated:YES];
            [self performSelector:@selector(removeView:) withObject:moviePlayer afterDelay:0.3];
        }
        
    }else{
        MPMoviePlayerController *moviePlayer = [aNotification object];
        [moviePlayer setFullscreen:NO animated:YES];
        [self performSelector:@selector(removeView:) withObject:moviePlayer afterDelay:0.3];
        
    }

    
}


-(void)removeView:(MPMoviePlayerController *)moviePlayer{
    MPMoviePlayerController *mPlayer = moviePlayer;
    [mPlayer.view removeFromSuperview];
}


-(void)playBackStateChanged:(id)sender
{
    
    MPMoviePlaybackState playbackState = [moviePlayerViewController.moviePlayer playbackState];
    
    switch (playbackState) {
            
        case MPMoviePlaybackStatePaused :
        {

            
            break;
        }
            
        case MPMoviePlaybackStateStopped :
        {
            userPressNext = YES;
            NSLog(@"MPMoviePlaybackStateStopped");
            UIAlertView *stoppedAlert = [[UIAlertView alloc]initWithTitle:@"Warning" message:[NSString stringWithFormat:@"You have not completed watching this video. If you exit now, you will not earn $%@",self.action.offer_value] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [stoppedAlert show];
            
            break;
        }
            
        case MPMoviePlaybackStatePlaying :
            

            break;
        
        case MPMoviePlaybackStateSeekingForward:
        {

            
            UIAlertView *forwardAlert = [[UIAlertView alloc]initWithTitle:@"Warning" message:[NSString stringWithFormat:@"You have not completed watching this video. If you exit now, you will not earn $%@",self.action.offer_value] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
           [forwardAlert show];
            
            
            break;
        }
            
        case MPMoviePlaybackStateInterrupted :

            
            
            break;
    }
}

-(void)PopupDidclose{
    [self.navigationController popViewControllerAnimated:YES];

}

@end
