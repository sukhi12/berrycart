//
//  SettingsViewController.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/27/14.
//  Copyright (c) 2014 BerryCart.. All rights reserved.
//

#import "SettingsViewController.h"
#import "SWRevealViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "BerryCart_API.h"
#import "MBProgressHUD.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Side Menu
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeName:) name:@"SuccessLinking" object:nil];

}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    [BerryCart_API getUserData:nil :^(NSDictionary *resul) {
                
        if ([NSString stringWithFormat:@"%@",resul[@"data"][@"facebook_id"]].length >0) {

            self.facebookLabel.text = @"Linked";
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)linktoFacebookAccount:(UIButton *)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    theAppDelegate.fromLogin = NO;
    
    NSLog(@"return %i",[theAppDelegate openSessionWithAllowLoginUI:YES]);
    
    
}

-(IBAction)logoutClicked
{
    AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    
     [[FBSession activeSession] close];
    
    [app closeSession];
    
    self.facebookLabel.text=@"Not Linked";
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) changeName: (NSNotification *) notif{
    
    NSDictionary *data = notif.object;
    [self linkTheUserFacebook:data];
    
}

- (void) linkTheUserFacebook: (NSDictionary *) data{
    
  //..........................My Coding..............................
    
    
        NSLog(@"%@",self.facebookLabel.text);
        if (FBSession.activeSession.isOpen &&[self.facebookLabel.text isEqualToString:@"Not Linked"] )
        {
                    self.facebookLabel.text = @"Linked";
        }
        else
        {
            [[[UIAlertView alloc]initWithTitle:nil message:@"The current Facebook account is already linked to another berrycart account." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        }
   
      [MBProgressHUD hideHUDForView:self.view animated:YES];
    
   //......................................*****.......................................
    
//    [BerryCart_API createSessionAction:^(BOOL hasSession) {
//        
//        if (hasSession) {
//            
//            [BerryCart_API addFacebookID:data[@"id"]];
//            
//            [BerryCart_API loginAction:@{@"session":[BerryCart_API user_session], @"facebook_id":[BerryCart_API facebookID]} completion:^(NSDictionary *resul) {
//                
//                [MBProgressHUD hideHUD];
//                
//                if ([resul[@"message"] isEqualToString:@"customer does not exist"]) {
//                    
//                    NSMutableDictionary *param = [NSMutableDictionary dictionary];
//                    [param setObject:data[@"id"] forKey:@"facebook_id"];
//                    //param setObject:theAppDelegate.loggedInUser[@"data"][@"Debug"][[@"CustomerId"] forKey:@"id"];
//                    
//                    [BerryCart_API updateUser:param :^(NSDictionary *resul) {
//                        self.facebookLabel.text = data[@"email"];
//                        
//                        NSLog(@"%@",self.facebookLabel.text);
//                        NSLog(@"resultt :%@", resul);
//                        
//                    }];
//
//                    
//                    
//                    
//                }else if ([[NSString stringWithFormat:@"%@",resul[@"message"]] isEqualToString:@"success"]){
//                    
//                    [[[UIAlertView alloc]initWithTitle:nil message:@"The current Facebook account is already linked to another berrycart account." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
//                    
//                }
//                
//                
//                
//                
//            }];
//            
//            
//        }else{
//            
//            
//        }
//        
//    }];
//    
    
    

}

@end
