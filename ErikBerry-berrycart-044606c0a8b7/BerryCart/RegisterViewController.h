//
//  LoginViewController.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/15/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailAddressTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UITextField *reenterPassword;

@property (nonatomic, strong) NSString *faceBookId;

- (IBAction)backButton:(UIButton *)sender;
- (IBAction)nextTapped:(UIButton *)sender;
- (IBAction)goBack:(UIButton *)sender;
- (IBAction)toPrivacyLink:(UIButton *)sender;

@end
