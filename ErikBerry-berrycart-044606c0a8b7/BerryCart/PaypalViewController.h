//
//  PaypalViewController.h
//  BerryCart
//
//  Created by Rommel Bulalacao on 1/16/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaypalViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *view1;
- (IBAction)goBack:(UIButton *)sender;
- (IBAction)submitTapped:(UIButton *)sender;
- (IBAction)createAccountAtPayPal:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *paypalEmailAddressTextField;

@end
