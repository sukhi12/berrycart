//
//  Action_Survey_Answer.m
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import "Action_Survey_Answer.h"
#import "Action_Rule.h"


@implementation Action_Survey_Answer

@dynamic action_rule_id;
@dynamic action_survey_answer_id;
@dynamic answer;
@dynamic action_rule;

@end
