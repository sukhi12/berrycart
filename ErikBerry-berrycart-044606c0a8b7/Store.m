//
//  Store.m
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import "Store.h"
#import "Ad.h"


@implementation Store

@dynamic address;
@dynamic app_id;
@dynamic city;
@dynamic created;
@dynamic created_by;
@dynamic deleted_flag;
@dynamic latitude;
@dynamic longitude;
@dynamic lowest_sync_uniq_id;
@dynamic max_coupons_pushed_per_visit;
@dynamic most_recent_sync_uniq_id;
@dynamic parameters;
@dynamic parent_store_id;
@dynamic phone;
@dynamic position;
@dynamic state;
@dynamic status;
@dynamic store_id;
@dynamic store_name;
@dynamic sync_uniq_id;
@dynamic updated;
@dynamic zip;
@dynamic ad;

@end
