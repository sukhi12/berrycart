//
//  Store.h
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Ad;

@interface Store : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * app_id;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSNumber * created_by;
@property (nonatomic, retain) NSNumber * deleted_flag;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSNumber * lowest_sync_uniq_id;
@property (nonatomic, retain) NSNumber * max_coupons_pushed_per_visit;
@property (nonatomic, retain) NSNumber * most_recent_sync_uniq_id;
@property (nonatomic, retain) NSString * parameters;
@property (nonatomic, retain) NSNumber * parent_store_id;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSNumber * position;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSNumber * store_id;
@property (nonatomic, retain) NSString * store_name;
@property (nonatomic, retain) NSNumber * sync_uniq_id;
@property (nonatomic, retain) NSDate * updated;
@property (nonatomic, retain) NSString * zip;
@property (nonatomic, retain) Ad *ad;

@end
