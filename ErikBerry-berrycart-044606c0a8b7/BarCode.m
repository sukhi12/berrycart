//
//  BarCode.m
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import "BarCode.h"
#import "Ad.h"


@implementation BarCode

@dynamic ad_id;
@dynamic barcode;
@dynamic barcode_id;
@dynamic ad;

@end
