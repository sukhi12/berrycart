//
//  BarCode.h
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Ad;

@interface BarCode : NSManagedObject

@property (nonatomic, retain) NSNumber * ad_id;
@property (nonatomic, retain) NSString * barcode;
@property (nonatomic, retain) NSNumber * barcode_id;
@property (nonatomic, retain) Ad *ad;

@end
