//
//  Action.m
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import "Action.h"
#import "Action_Rule.h"
#import "Ad.h"


@implementation Action

@dynamic action_id;
@dynamic action_type;
@dynamic ad_id;
@dynamic fb_description;
@dynamic fb_link;
@dynamic fb_name;
@dynamic image;
@dynamic line1;
@dynamic max_uses;
@dynamic max_uses_per_day;
@dynamic offer_value;
@dynamic question;
@dynamic shuffle_flag;
@dynamic survey_type;
@dynamic validation_required;
@dynamic video_url;
@dynamic ad;
@dynamic action_rule;

@end
