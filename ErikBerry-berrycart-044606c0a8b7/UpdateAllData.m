//
//  UpdateAllData.m
//  BerryCart
//
//  Created by Romeo Flauta on 2/3/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//


#import "UpdateAllData.h"
#import "BerryCart_API.h"
#import "Ad.h"
#import "Store.h"
#import "Action.h"
#import "Add_Store_Map.h"
#import "BarCode.h"
#import "Action_Rule.h"
#import "Action_Survey_Answer.h"
#import "AppDelegate.h"

//AFnetworking
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "AFNetworking.h"
#import "AFURLConnectionOperation.h"
#import "JSON.h"

NSOperationQueue *queue;
NSDateFormatter *f;
NSNull *isNUll;

@implementation UpdateAllData

//update add




+(void)startupdateAllData{
    NSLog(@"startupdateAllData");
    if (queue== nil) {
        queue = [[NSOperationQueue alloc]init];
       f =[[NSDateFormatter alloc]init];
        [f setDateFormat:@"yyyy-MM-dd HH:m:s"];
        isNUll = [NSNull null];
    }

    [UpdateAllData updateAd];
    
}




+(void)updateadWithBlock:(void(^)(NSDictionary * dict))block{
    
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/ads.json" parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue ];
         
         block(dict);
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
     }
     ];
    
    [queue addOperation:operation];
}


//Action
+(void)updateActionWithBlock:(void(^)(NSDictionary * dict))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/actions.json" parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue ];
         
         block(dict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
     }
     ];
    
    [queue addOperation:operation];
}

//Store
+(void)updateStoreWithBlock:(void(^)(NSDictionary * dict))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/stores.json" parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue ];
         
         block(dict);

     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
     }
     ];
    
    [queue addOperation:operation];
    
}


//action_rule
+(void)updateActionRuleWithBlock:(void(^)(NSDictionary * dict))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/action_rules.json" parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue ];
         
         block(dict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
     }
     ];
    
    [queue addOperation:operation];
    
}


//action survey answer
+(void)updateActionSurveyAnswerWithBlock:(void(^)(NSDictionary * dict))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/action_survey_answers.json" parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue ];
         
         block(dict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
     }
     ];
    
    [queue addOperation:operation];
    
}





#pragma No - Blocks
+(void)updateAd{
    
    [UpdateAllData updateadWithBlock:^(NSDictionary * dict) {// AD
        
        
        NSArray *fetchedObjects = [UpdateAllData fetchwithEntityName:@"Ad"];
        
        if (fetchedObjects.count > 0 ) {
            
            [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *addDcit, NSUInteger idx, BOOL *stop) {
                
                NSPredicate *predicateID = [NSPredicate predicateWithFormat:@"ad_id = %i",[dict[@"id"] intValue]];
                NSArray *arrayWithPredicate = [fetchedObjects filteredArrayUsingPredicate:predicateID];
                
                if (arrayWithPredicate.count == 0) {
                    
                    Ad *addAd = [NSEntityDescription insertNewObjectForEntityForName:@"Ad" inManagedObjectContext:theAppDelegate.managedObjectContext];
                    
                    addAd.activation_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"activation_date"]]];
                    addAd.ad_id = [NSNumber numberWithInt:[addDcit[@"id"]intValue]];
                    addAd.customer_segment = addDcit[@"segment_id"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"segment_id"]intValue]] : [NSNumber numberWithInt:0];
                    addAd.expiration_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"expiration_date"]]];
                    addAd.image = addDcit[@"image"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"image"]] : @"";
                    addAd.line1 = addDcit[@"line1"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line1"]] : @"";
                    addAd.line2 = addDcit[@"line2"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line2"]] : @"";
                    addAd.max_uses = addDcit[@"max_uses"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses"]intValue]] : [NSNumber numberWithInt:0];
                    addAd.max_uses_per_day = addDcit[@"max_uses_per_day"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses_per_day"]intValue]] : [NSNumber numberWithInt:0];
                    addAd.product_UPCs = addDcit[@"product_UPCs"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"product_UPCs"]] : @"";
                    addAd.title = addDcit[@"title"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"title"]] : @"";
                    
                }else{
                    
                    [arrayWithPredicate enumerateObjectsUsingBlock:^(Ad *addAd, NSUInteger idx, BOOL *stop) {
                        
                        addAd.activation_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"activation_date"]]];
                        addAd.ad_id = [NSNumber numberWithInt:[addDcit[@"id"]intValue]];
                        addAd.customer_segment = addDcit[@"segment_id"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"segment_id"]intValue]] : [NSNumber numberWithInt:0];
                        addAd.expiration_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"expiration_date"]]];
                        addAd.image = addDcit[@"image"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"image"]] : @"";
                        addAd.line1 = addDcit[@"line1"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line1"]] : @"";
                        addAd.line2 = addDcit[@"line2"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line2"]] : @"";
                        addAd.max_uses = addDcit[@"max_uses"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses"]intValue]] : [NSNumber numberWithInt:0];
                        addAd.max_uses_per_day = addDcit[@"max_uses_per_day"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses_per_day"]intValue]] : [NSNumber numberWithInt:0];
                        addAd.product_UPCs = addDcit[@"product_UPCs"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"product_UPCs"]] : @"";
                        addAd.title = addDcit[@"title"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"title"]] : @"";
                        
                    }];
                }
                
            }];
            
        }else{
            
            [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *addDcit, NSUInteger idx, BOOL *stop) {
                Ad *addAd = [NSEntityDescription insertNewObjectForEntityForName:@"Ad" inManagedObjectContext:theAppDelegate.managedObjectContext];
                
                addAd.activation_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"activation_date"]]];
                addAd.ad_id = [NSNumber numberWithInt:[addDcit[@"id"]intValue]];
                addAd.customer_segment = addDcit[@"segment_id"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"segment_id"]intValue]] : [NSNumber numberWithInt:0];
                addAd.expiration_date = [f dateFromString:[NSString stringWithFormat:@"%@",addDcit[@"expiration_date"]]];
                addAd.image = addDcit[@"image"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"image"]] : @"";
                addAd.line1 = addDcit[@"line1"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line1"]] : @"";
                addAd.line2 = addDcit[@"line2"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"line2"]] : @"";
                addAd.max_uses = addDcit[@"max_uses"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses"]intValue]] : [NSNumber numberWithInt:0];
                addAd.max_uses_per_day = addDcit[@"max_uses_per_day"] != isNUll ? [NSNumber numberWithInt:[addDcit[@"max_uses_per_day"]intValue]] : [NSNumber numberWithInt:0];
                addAd.product_UPCs = addDcit[@"product_UPCs"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"product_UPCs"]] : @"";
                addAd.title = addDcit[@"title"] != isNUll ? [NSString stringWithFormat:@"%@",addDcit[@"title"]] : @"";
                
                
            }];
            
        }
        
        [theAppDelegate saveContext];
        
        //calling method to update data
        [UpdateAllData updateStore];
        [UpdateAllData updateAction];
        
        
    }];
    
}
+(void)updateStore{
    
    [UpdateAllData updateStoreWithBlock:^(NSDictionary *dict) { // get Store
        NSArray *fetchObject = [UpdateAllData fetchwithEntityName:@"Store"];
        
        
        if (fetchObject.count > 0) {
            
            
            [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *storeDict, NSUInteger idx, BOOL *stop) {
                NSPredicate *predicateID = [NSPredicate predicateWithFormat:@"store_id = %i",[[NSString stringWithFormat:@"%@",storeDict[@"id"]]intValue]];
                NSArray *arrayPridicate = [fetchObject filteredArrayUsingPredicate:predicateID];
                
                
                if(arrayPridicate.count == 0){
                    
                    
                    Store *addStore = [NSEntityDescription insertNewObjectForEntityForName:@"Store" inManagedObjectContext:theAppDelegate.managedObjectContext];
                    
                    addStore.store_name = storeDict[@"address"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"address"]] : @"";
                    addStore.app_id = storeDict[@"app_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"app_id"]intValue]] : [NSNumber numberWithInt:0];
                    addStore.city = storeDict[@"city"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"city"]] : @"";
                    addStore.created = [NSDate dateWithTimeIntervalSince1970:[storeDict[@"created"] intValue]];
                    addStore.deleted_flag = storeDict[@"created_by"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"deleted_flag"]intValue]] : [NSNumber numberWithInt:0];
                    addStore.store_id = [NSNumber numberWithInt:[storeDict[@"id"]intValue]];
                    addStore.latitude = storeDict[@"latitude"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"latitude"]] : @"";
                    addStore.longitude = storeDict[@"longitude"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"longitude"]] : @"";
                    addStore.lowest_sync_uniq_id = storeDict[@"lowest_sync_uniq_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"lowest_sync_uniq_id"]intValue]] : [NSNumber numberWithInt:0];
                    addStore.max_coupons_pushed_per_visit = storeDict[@"max_coupons_pushed_per_visit"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"max_coupons_pushed_per_visit"]intValue]] : [NSNumber numberWithInt:0];
                    addStore.most_recent_sync_uniq_id = storeDict[@"most_recent_sync_uniq_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"most_recent_sync_uniq_id"]intValue]] : [NSNumber numberWithInt:0];
                    addStore.parameters = storeDict[@"parameters"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"parameters"]] : @"";
                    addStore.parent_store_id = storeDict[@"parent_store_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"parent_store_id"]intValue]] : [NSNumber numberWithInt:0];
                    addStore.parameters = storeDict[@"phone"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"phone"]] : @"";
                    addStore.position = storeDict[@"position"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"position"]intValue]] : [NSNumber numberWithInt:0];
                    addStore.state = storeDict[@"state"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"state"]] : @"";
                    addStore.status = storeDict[@"status"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"status"]intValue]] : [NSNumber numberWithInt:0];
                    addStore.store_name = storeDict[@"store_name"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"store_name"]] : @"";
                    addStore.sync_uniq_id = storeDict[@"sync_uniq_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"sync_uniq_id"]intValue]] : [NSNumber numberWithInt:0];
                    addStore.updated = [NSDate dateWithTimeIntervalSince1970:[storeDict[@"updated"] intValue]];
                    addStore.zip = storeDict[@"zip"] !=isNUll ? [NSString stringWithFormat:@"%@ ",storeDict[@"zip"]] : @"";
                    
                    
                    
                }else{
                    
                    [arrayPridicate enumerateObjectsUsingBlock:^(Store *addStore, NSUInteger idx, BOOL *stop) {
                        addStore.store_name = storeDict[@"address"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"address"]] : @"";
                        addStore.app_id = storeDict[@"app_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"app_id"]intValue]] : [NSNumber numberWithInt:0];
                        addStore.city = storeDict[@"city"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"city"]] : @"";
                        addStore.created = [NSDate dateWithTimeIntervalSince1970:[storeDict[@"created"] intValue]];
                        addStore.deleted_flag = storeDict[@"created_by"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"deleted_flag"]intValue]] : [NSNumber numberWithInt:0];
                        addStore.store_id = [NSNumber numberWithInt:[storeDict[@"id"]intValue]];
                        addStore.latitude = storeDict[@"latitude"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"latitude"]] : @"";
                        addStore.longitude = storeDict[@"longitude"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"longitude"]] : @"";
                        addStore.lowest_sync_uniq_id = storeDict[@"lowest_sync_uniq_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"lowest_sync_uniq_id"]intValue]] : [NSNumber numberWithInt:0];
                        addStore.max_coupons_pushed_per_visit = storeDict[@"max_coupons_pushed_per_visit"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"max_coupons_pushed_per_visit"]intValue]] : [NSNumber numberWithInt:0];
                        addStore.most_recent_sync_uniq_id = storeDict[@"most_recent_sync_uniq_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"most_recent_sync_uniq_id"]intValue]] : [NSNumber numberWithInt:0];
                        addStore.parameters = storeDict[@"parameters"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"parameters"]] : @"";
                        addStore.parent_store_id = storeDict[@"parent_store_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"parent_store_id"]intValue]] : [NSNumber numberWithInt:0];
                        addStore.parameters = storeDict[@"phone"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"phone"]] : @"";
                        addStore.position = storeDict[@"position"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"position"]intValue]] : [NSNumber numberWithInt:0];
                        addStore.state = storeDict[@"state"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"state"]] : @"";
                        addStore.status = storeDict[@"status"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"status"]intValue]] : [NSNumber numberWithInt:0];
                        addStore.store_name = storeDict[@"store_name"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"store_name"]] : @"";
                        addStore.sync_uniq_id = storeDict[@"sync_uniq_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"sync_uniq_id"]intValue]] : [NSNumber numberWithInt:0];
                        addStore.updated = [NSDate dateWithTimeIntervalSince1970:[storeDict[@"updated"] intValue]];
                        addStore.zip = storeDict[@"zip"] !=isNUll ? [NSString stringWithFormat:@"%@ ",storeDict[@"zip"]] : @"";
                        
                    }];
                    
                    
                }
                
                
                
            }];
            
            
        }else{
            
            [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *storeDict, NSUInteger idx, BOOL *stop) {
                
                Store *addStore = [NSEntityDescription insertNewObjectForEntityForName:@"Store" inManagedObjectContext:theAppDelegate.managedObjectContext];
                
                addStore.store_name = storeDict[@"address"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"address"]] : @"";
                addStore.app_id = storeDict[@"app_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"app_id"]intValue]] : [NSNumber numberWithInt:0];
                addStore.city = storeDict[@"city"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"city"]] : @"";
                addStore.created = [NSDate dateWithTimeIntervalSince1970:[storeDict[@"created"] intValue]];
                addStore.deleted_flag = storeDict[@"created_by"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"deleted_flag"]intValue]] : [NSNumber numberWithInt:0];
                addStore.store_id = [NSNumber numberWithInt:[storeDict[@"id"]intValue]];
                addStore.latitude = storeDict[@"latitude"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"latitude"]] : @"";
                addStore.longitude = storeDict[@"longitude"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"longitude"]] : @"";
                addStore.lowest_sync_uniq_id = storeDict[@"lowest_sync_uniq_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"lowest_sync_uniq_id"]intValue]] : [NSNumber numberWithInt:0];
                addStore.max_coupons_pushed_per_visit = storeDict[@"max_coupons_pushed_per_visit"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"max_coupons_pushed_per_visit"]intValue]] : [NSNumber numberWithInt:0];
                addStore.most_recent_sync_uniq_id = storeDict[@"most_recent_sync_uniq_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"most_recent_sync_uniq_id"]intValue]] : [NSNumber numberWithInt:0];
                addStore.parameters = storeDict[@"parameters"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"parameters"]] : @"";
                addStore.parent_store_id = storeDict[@"parent_store_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"parent_store_id"]intValue]] : [NSNumber numberWithInt:0];
                addStore.parameters = storeDict[@"phone"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"phone"]] : @"";
                addStore.position = storeDict[@"position"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"position"]intValue]] : [NSNumber numberWithInt:0];
                addStore.state = storeDict[@"state"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"state"]] : @"";
                addStore.status = storeDict[@"status"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"status"]intValue]] : [NSNumber numberWithInt:0];
                addStore.store_name = storeDict[@"store_name"] !=isNUll ? [NSString stringWithFormat:@"%@",storeDict[@"store_name"]] : @"";
                addStore.sync_uniq_id = storeDict[@"sync_uniq_id"] !=isNUll ? [NSNumber numberWithInt:[storeDict[@"sync_uniq_id"]intValue]] : [NSNumber numberWithInt:0];
                addStore.updated = [NSDate dateWithTimeIntervalSince1970:[storeDict[@"updated"] intValue]];
                addStore.zip = storeDict[@"zip"] !=isNUll ? [NSString stringWithFormat:@"%@ ",storeDict[@"zip"]] : @"";
                
                
            }];
            
        }
        
        
        [theAppDelegate saveContext];
        
        
        
    }];

    
}
+(void)updateAction{
    
    //updateActionblock
    [UpdateAllData updateActionWithBlock:^(NSDictionary *dict) {//update action
        
        
        NSArray *fetchAdObject = [UpdateAllData fetchwithEntityName:@"Ad"];
        NSArray *fetchedObjects = [UpdateAllData fetchwithEntityName:@"Action"];
        
        
        if (fetchedObjects.count >0) {
            
            [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *actionDcit, NSUInteger idx, BOOL *stop) {
                
                NSPredicate *predicateID = [NSPredicate predicateWithFormat:@"action_id = %i",[[NSString stringWithFormat:@"%@",actionDcit[@"id"]]intValue]];
                NSArray *arrayPredicate = [fetchedObjects filteredArrayUsingPredicate:predicateID];
                
                if (arrayPredicate.count == 0) {
                    
                    NSPredicate *predicateWithAdID = [NSPredicate predicateWithFormat:@"ad_id = %i",[[NSString stringWithFormat:@"%@",actionDcit[@"ad_id"]]intValue]];
                    NSArray *adArray = [fetchAdObject filteredArrayUsingPredicate:predicateWithAdID];
                    
                    
                    Action *addAction = [NSEntityDescription  insertNewObjectForEntityForName:@"Action" inManagedObjectContext:theAppDelegate.managedObjectContext];
                    
                    addAction.action_id = [NSNumber numberWithInt:[actionDcit[@"id"]intValue]];
                    addAction.action_type = actionDcit[@"action_type"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"action_type"]] : @"";
                    addAction.ad_id = [NSNumber numberWithInt:[actionDcit[@"ad_id"]intValue]];
                    addAction.fb_description = actionDcit[@"fb_description"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"fb_description"]] : @"";
                    addAction.fb_link = actionDcit[@"fb_link"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"fb_link"]] : @"";
                    addAction.fb_name = actionDcit[@"fb_name"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"fb_name"]] : @"";
                    addAction.image = actionDcit[@"image"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"image"]] : @"";
                    addAction.line1 = actionDcit[@"line1"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"line1"]] : @"";
                    addAction.max_uses = actionDcit[@"max_uses"] != isNUll ? [NSNumber numberWithInt:[actionDcit[@"max_uses"]intValue]] : [NSNumber numberWithInt:0];
                    addAction.max_uses_per_day = actionDcit[@"max_uses_per_day"] != isNUll ? [NSNumber numberWithInt:[actionDcit[@"max_uses_per_day"]intValue]] : [NSNumber numberWithInt:0];
                    addAction.offer_value = actionDcit[@"offer_value"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"offer_value"]] : @"";
                    addAction.question = actionDcit[@"question"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"question"]] : @"";
                    addAction.shuffle_flag = actionDcit[@"shuffle_flag"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"shuffle_flag"]] : @"";
                    addAction.validation_required = actionDcit[@"validation"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"validation"]] : @"";
                    addAction.video_url = actionDcit[@"video_url"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"video_url"]] : @"";
                    addAction.survey_type = actionDcit[@"survey_type"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"survey_type"]] : @"";
                   
                    if (adArray.count > 0) {
                        Ad *oneAd = adArray[0];
                        [oneAd addActionsObject:addAction];
                    }
                    
                   
                    
                }else{
                    
                    [arrayPredicate enumerateObjectsUsingBlock:^(Action *addAction, NSUInteger idx, BOOL *stop) {
                        
                        addAction.action_id = [NSNumber numberWithInt:[actionDcit[@"id"]intValue]];
                        addAction.action_type = actionDcit[@"action_type"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"action_type"]] : @"";
                        addAction.ad_id = [NSNumber numberWithInt:[actionDcit[@"ad_id"]intValue]];
                        addAction.fb_description = actionDcit[@"fb_description"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"fb_description"]] : @"";
                        addAction.fb_link = actionDcit[@"fb_link"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"fb_link"]] : @"";
                        addAction.fb_name = actionDcit[@"fb_name"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"fb_name"]] : @"";
                        addAction.image = actionDcit[@"image"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"image"]] : @"";
                        addAction.line1 = actionDcit[@"line1"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"line1"]] : @"";
                        addAction.max_uses = actionDcit[@"max_uses"] != isNUll ? [NSNumber numberWithInt:[actionDcit[@"max_uses"]intValue]] : [NSNumber numberWithInt:0];
                        addAction.max_uses_per_day = actionDcit[@"max_uses_per_day"] != isNUll ? [NSNumber numberWithInt:[actionDcit[@"max_uses_per_day"]intValue]] : [NSNumber numberWithInt:0];
                        addAction.offer_value = actionDcit[@"offer_value"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"offer_value"]] : @"";
                        addAction.question = actionDcit[@"question"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"question"]] : @"";
                        addAction.shuffle_flag = actionDcit[@"shuffle_flag"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"shuffle_flag"]] : @"";
                        addAction.validation_required = actionDcit[@"validation"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"validation"]] : @"";
                        addAction.video_url = actionDcit[@"video_url"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"video_url"]] : @"";
                        addAction.survey_type = actionDcit[@"survey_type"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"survey_type"]] : @"";
                    }];
                }
                
                
                
                
            }];
            
        }else{
            
            [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *actionDcit, NSUInteger idx, BOOL *stop) {
                
                NSPredicate *predicateWithAdID = [NSPredicate predicateWithFormat:@"ad_id = %i",[[NSString stringWithFormat:@"%@",actionDcit[@"ad_id"]]intValue]];
                NSArray *adArray = [fetchAdObject filteredArrayUsingPredicate:predicateWithAdID];
                
                Action *addAction = [NSEntityDescription  insertNewObjectForEntityForName:@"Action" inManagedObjectContext:theAppDelegate.managedObjectContext];
                
                addAction.action_id = [NSNumber numberWithInt:[actionDcit[@"id"]intValue]];
                addAction.action_type = actionDcit[@"action_type"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"action_type"]] : @"";
                addAction.ad_id = [NSNumber numberWithInt:[actionDcit[@"ad_id"]intValue]];
                addAction.fb_description = actionDcit[@"fb_description"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"fb_description"]] : @"";
                addAction.fb_link = actionDcit[@"fb_link"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"fb_link"]] : @"";
                addAction.fb_name = actionDcit[@"fb_name"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"fb_name"]] : @"";
                addAction.image = actionDcit[@"image"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"image"]] : @"";
                addAction.line1 = actionDcit[@"line1"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"line1"]] : @"";
                addAction.max_uses = actionDcit[@"max_uses"] != isNUll ? [NSNumber numberWithInt:[actionDcit[@"max_uses"]intValue]] : [NSNumber numberWithInt:0];
                addAction.max_uses_per_day = actionDcit[@"max_uses_per_day"] != isNUll ? [NSNumber numberWithInt:[actionDcit[@"max_uses_per_day"]intValue]] : [NSNumber numberWithInt:0];
                addAction.offer_value = actionDcit[@"offer_value"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"offer_value"]] : @"";
                addAction.question = actionDcit[@"question"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"question"]] : @"";
                addAction.shuffle_flag = actionDcit[@"shuffle_flag"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"shuffle_flag"]] : @"";
                addAction.validation_required = actionDcit[@"validation"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"validation"]] : @"";
                addAction.video_url = actionDcit[@"video_url"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"video_url"]] : @"";
                addAction.survey_type = actionDcit[@"survey_type"] != isNUll ? [NSString stringWithFormat:@"%@",actionDcit[@"survey_type"]] : @"";
               
                if(adArray.count > 0){
                    
                    Ad *oneAd = adArray[0];
                    [oneAd addActionsObject:addAction];
                }
                
                
            }];
            
        }
        
        [theAppDelegate saveContext];
        
        //and calling update action ruke
        [UpdateAllData updateActionrule];
        
    }];
    
}
+(void)updateActionrule{
    
    [UpdateAllData updateActionRuleWithBlock:^(NSDictionary *dict) {
        NSArray *actionruleObjectArray = [UpdateAllData fetchwithEntityName:@"Action_Rule"];
        NSArray *actionObjectArray = [UpdateAllData fetchwithEntityName:@"Action"];
        
        if (actionruleObjectArray.count >0) {
            
            [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *actionrule, NSUInteger idx, BOOL *stop) {
               
                NSPredicate *predicateID = [NSPredicate predicateWithFormat:@"action_rule = %i",[[NSString stringWithFormat:@"%@",actionrule[@"id"]]intValue]];
                NSArray *predicateArray = [actionruleObjectArray filteredArrayUsingPredicate:predicateID];
                
                if (predicateArray.count == 0) {
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"action_id = %i",[[NSString stringWithFormat:@"%@",actionrule[@"action_id"]]intValue]];
                    NSArray *actionArray = [actionObjectArray filteredArrayUsingPredicate:predicate];
                    
                    
                    Action_Rule *addActionRule = [NSEntityDescription insertNewObjectForEntityForName:@"Action_Rule" inManagedObjectContext:theAppDelegate.managedObjectContext];
                    
                    addActionRule.action_rule = [NSNumber numberWithInt:[actionrule[@"id"]intValue]];
                    addActionRule.action_id = [NSNumber numberWithInt:[actionrule[@"action_id"]intValue]];
                    addActionRule.action_rule_description = actionrule[@"description"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"description"]] : @"";
                    addActionRule.video_url = actionrule[@"video_url"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"video_url"]] : @"";
                    addActionRule.video_thumbnail = actionrule[@"video_thumbnail"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"video_thumbnail"]] : @"";
                    addActionRule.survey_type = [NSNumber numberWithInt:[actionrule[@"survey_type"]intValue]];
                    addActionRule.question = actionrule[@"question"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"question"]] : @"";
                    addActionRule.shuffle_flag = [NSNumber numberWithInt:[actionrule[@"shuffle_flag"]intValue]];
                    addActionRule.fb_link = actionrule[@"fb_link"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_link"]] : @"";
                    addActionRule.fb_name = actionrule[@"fb_name"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_name"]] : @"";
                    addActionRule.fb_caption = actionrule[@"fb_caption"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_caption"]] : @"";
                    addActionRule.fb_image = actionrule[@"fb_image"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_image"]] : @"";
                    
                    
                    if (actionArray.count >0) {
                        Action *oneAction = actionArray[0];
                        oneAction.action_rule = addActionRule;
                    }

                }else{
                    
                    [predicateArray enumerateObjectsUsingBlock:^(Action_Rule *addActionRule, NSUInteger idx, BOOL *stop) {
                        
                        addActionRule.action_rule = [NSNumber numberWithInt:[actionrule[@"id"]intValue]];
                        addActionRule.action_id = [NSNumber numberWithInt:[actionrule[@"action_id"]intValue]];
                        addActionRule.action_rule_description = actionrule[@"description"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"description"]] : @"";
                        addActionRule.video_url = actionrule[@"video_url"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"video_url"]] : @"";
                        addActionRule.video_thumbnail = actionrule[@"video_thumbnail"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"video_thumbnail"]] : @"";
                        addActionRule.survey_type = [NSNumber numberWithInt:[actionrule[@"survey_type"]intValue]];
                        addActionRule.question = actionrule[@"question"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"question"]] : @"";
                        addActionRule.shuffle_flag = [NSNumber numberWithInt:[actionrule[@"shuffle_flag"]intValue]];
                        addActionRule.fb_link = actionrule[@"fb_link"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_link"]] : @"";
                        addActionRule.fb_name = actionrule[@"fb_name"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_name"]] : @"";
                        addActionRule.fb_caption = actionrule[@"fb_caption"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_caption"]] : @"";
                        addActionRule.fb_image = actionrule[@"fb_image"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_image"]] : @"";
                        
                    }];
                    
                }
            }];

            
            
            
        }else{
            
            [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *actionrule, NSUInteger idx, BOOL *stop) {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"action_id = %i",[[NSString stringWithFormat:@"%@",actionrule[@"action_id"]]intValue]];
                NSArray *actionArray = [actionObjectArray filteredArrayUsingPredicate:predicate];
                
                
                Action_Rule *addActionRule = [NSEntityDescription insertNewObjectForEntityForName:@"Action_Rule" inManagedObjectContext:theAppDelegate.managedObjectContext];
                
                addActionRule.action_rule = [NSNumber numberWithInt:[actionrule[@"id"]intValue]];
                addActionRule.action_id = [NSNumber numberWithInt:[actionrule[@"action_id"]intValue]];
                addActionRule.action_rule_description = actionrule[@"description"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"description"]] : @"";
                addActionRule.video_url = actionrule[@"video_url"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"video_url"]] : @"";
                addActionRule.video_thumbnail = actionrule[@"video_thumbnail"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"video_thumbnail"]] : @"";
                addActionRule.survey_type = [NSNumber numberWithInt:[actionrule[@"survey_type"]intValue]];
                addActionRule.question = actionrule[@"question"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"question"]] : @"";
                addActionRule.shuffle_flag = [NSNumber numberWithInt:[actionrule[@"shuffle_flag"]intValue]];
                addActionRule.fb_link = actionrule[@"fb_link"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_link"]] : @"";
                addActionRule.fb_name = actionrule[@"fb_name"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_name"]] : @"";
                addActionRule.fb_caption = actionrule[@"fb_caption"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_caption"]] : @"";
                addActionRule.fb_image = actionrule[@"fb_image"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"fb_image"]] : @"";
                if (actionArray.count > 0) {
                    Action *oneAction = actionArray[0];
                    oneAction.action_rule = addActionRule;
                }
             
            }];
            
            
        }
        
        
        [theAppDelegate saveContext];
        
        [UpdateAllData updateActionActionAnswer];
    }];
    
    
    
    
}
+(void)updateActionActionAnswer{
    
    [UpdateAllData updateActionSurveyAnswerWithBlock:^(NSDictionary *dict) {
        NSArray *actionRuleArray = [UpdateAllData fetchwithEntityName:@"Action_Rule"];
        NSArray *actionSurveyQuestionArray = [UpdateAllData fetchwithEntityName:@"Action_Survey_Answer"];
        
        if (actionSurveyQuestionArray.count > 0) {
            [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *actionrule, NSUInteger idx, BOOL *stop) {
                NSPredicate *predicateID = [NSPredicate predicateWithFormat:@"action_survey_answer_id = %i",[[NSString stringWithFormat:@"%@",actionrule[@"id"]]intValue]];
                NSArray *actionsurveyPredicateArray = [actionSurveyQuestionArray filteredArrayUsingPredicate:predicateID];
                
                if (actionsurveyPredicateArray.count == 0) {
                    
                    NSPredicate *actionRulePredicate = [NSPredicate predicateWithFormat:@"action_rule = %i",[[NSString stringWithFormat:@"%@",actionrule[@"action_rule_id"]]intValue]];
                    NSArray *actionruleWithPredicateArray = [actionRuleArray filteredArrayUsingPredicate:actionRulePredicate];
                    
                    
                    Action_Survey_Answer *addActionSurveyAnswer = [NSEntityDescription insertNewObjectForEntityForName:@"Action_Survey_Answer" inManagedObjectContext:theAppDelegate.managedObjectContext];
                    addActionSurveyAnswer.action_survey_answer_id = [NSNumber numberWithInt:[actionrule[@"id"]intValue]];
                    addActionSurveyAnswer.action_rule_id = [NSNumber numberWithInt:[actionrule[@"action_rule_id"]intValue]];
                    addActionSurveyAnswer.answer = actionrule[@"answer"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"answer"]] : @"";
                    
                    if (actionruleWithPredicateArray.count > 0) {
                        Action_Rule *oneActionRule = actionruleWithPredicateArray[0];
                        [oneActionRule addAction_survey_answersObject:addActionSurveyAnswer];
                    }
                    
                }else{
                    
                    [actionsurveyPredicateArray enumerateObjectsUsingBlock:^(Action_Survey_Answer *addActionSurveyAnswer, NSUInteger idx, BOOL *stop) {
                        
                        addActionSurveyAnswer.action_survey_answer_id = [NSNumber numberWithInt:[actionrule[@"id"]intValue]];
                        addActionSurveyAnswer.action_rule_id = [NSNumber numberWithInt:[actionrule[@"action_rule_id"]intValue]];
                        addActionSurveyAnswer.answer = actionrule[@"answer"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"answer"]] : @"";
                        
                    }];
                }

            }];
            
            
            
        }else{
            
            [(NSArray *)dict[@"data"] enumerateObjectsUsingBlock:^(NSDictionary *actionrule, NSUInteger idx, BOOL *stop) {
                NSPredicate *actionRulePredicate = [NSPredicate predicateWithFormat:@"action_rule = %i",[[NSString stringWithFormat:@"%@",actionrule[@"action_rule_id"]]intValue]];
                NSArray *actionruleWithPredicateArray = [actionRuleArray filteredArrayUsingPredicate:actionRulePredicate];
                
                
                Action_Survey_Answer *addActionSurveyAnswer = [NSEntityDescription insertNewObjectForEntityForName:@"Action_Survey_Answer" inManagedObjectContext:theAppDelegate.managedObjectContext];
                addActionSurveyAnswer.action_survey_answer_id = [NSNumber numberWithInt:[actionrule[@"id"]intValue]];
                addActionSurveyAnswer.action_rule_id = [NSNumber numberWithInt:[actionrule[@"action_rule_id"]intValue]];
                addActionSurveyAnswer.answer = actionrule[@"answer"] != isNUll ? [NSString stringWithFormat:@"%@",actionrule[@"answer"]] : @"";
                
                if (actionruleWithPredicateArray.count > 0) {
                    Action_Rule *oneActionRule = actionruleWithPredicateArray[0];
                    [oneActionRule addAction_survey_answersObject:addActionSurveyAnswer];
                }
                
                
            }];

        }
        
        
        
        [theAppDelegate saveContext];
    }];
    
    
}




#pragma mark - coredata

+(NSArray *)fetchwithEntityName:(NSString *)name{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:name inManagedObjectContext:theAppDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;

    
    
    return [theAppDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
}

+(NSArray *)fetchwithEntityName:(NSString *)name withPredicateKey:(NSString *)pKey valueToSearch:(NSString *)value{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:name inManagedObjectContext:theAppDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %@",pKey,value]];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    return [theAppDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
}

@end
