//
//  UpdateAllData.h
//  BerryCart
//
//  Created by Romeo Flauta on 2/3/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpdateAllData : NSObject

+(void)startupdateAllData;

+(void)updateadWithBlock:(void(^)(NSDictionary * dict))block;

+(void)updateActionWithBlock:(void(^)(NSDictionary * dict))block;

+(void)updateStoreWithBlock:(void(^)(NSDictionary * dict))block;

+(void)updateAddStoreMapWithBlock:(void(^)(NSDictionary * dict))block;

+(void)updateActionRuleWithBlock:(void(^)(NSDictionary * dict))block;

+(void)updateActionSurveyAnswerWithBlock:(void(^)(NSDictionary * dict))block;




//noBlocks
+(void)updateAd;
+(void)updateStore;
+(void)updateAction;
+(void)updateActionrule;
+(void)updateActionActionAnswer;




//Codedata
+(NSArray *)fetchwithEntityName:(NSString *)name;
+(NSArray *)fetchwithEntityName:(NSString *)name withPredicateKey:(NSString *)pKey valueToSearch:(NSString *)value;


@end



