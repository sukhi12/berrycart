//
//  BerryCart_CoreData.m
//  BerryCart
//
//  Created by Romeo Flauta on 1/24/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "BerryCart_CoreData.h"

@implementation BerryCart_CoreData

+(void)fetchObjectsWithoutPredicate:(NSString *)entityName  completion:(void (^)(NSArray * resul))block{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:theAppDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [theAppDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        // No Fetched Objects
        NSLog(@"No fetched objects");
    }
    
    block(fetchedObjects);
    
}

+ (void)fetchObjectsWithPredicate:(NSString *)entityName andPredicateString:(NSString *)predicateString andPredicateArgument: (NSString *) predicateArgument completion:(void (^)(NSArray * result))block{
    

    NSLog(@"ENTITY NAME ==== %@", entityName);
    NSLog(@"PREDICATE STRING and ARGUMENT :%@ :%@", predicateString, predicateArgument);
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:theAppDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString, predicateArgument];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [theAppDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        // No fetched objects
        NSLog(@"No fetched objects");
    }
    
    if (error) {
        NSLog(@"ERROR :%@", error.localizedDescription);
    }
    
    block(fetchedObjects);

}

@end
