//
//  TimeStamp.h
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TimeStamp : NSManagedObject

@property (nonatomic, retain) NSString * server_time;

@end
