//
//  Ad.m
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import "Ad.h"
#import "Action.h"
#import "BarCode.h"
#import "Store.h"


@implementation Ad

@dynamic activation_date;
@dynamic ad_id;
@dynamic bc_coupons_store_map;
@dynamic customer_segment;
@dynamic expiration_date;
@dynamic image;
@dynamic line1;
@dynamic line2;
@dynamic max_uses;
@dynamic max_uses_per_day;
@dynamic product_UPCs;
@dynamic title;
@dynamic actions;
@dynamic barcodes;
@dynamic stores;

@end
