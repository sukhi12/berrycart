//
//  Ad.h
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action, BarCode, Store;

@interface Ad : NSManagedObject

@property (nonatomic, retain) NSDate * activation_date;
@property (nonatomic, retain) NSNumber * ad_id;
@property (nonatomic, retain) NSNumber * bc_coupons_store_map;
@property (nonatomic, retain) NSNumber * customer_segment;
@property (nonatomic, retain) NSDate * expiration_date;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * line1;
@property (nonatomic, retain) NSString * line2;
@property (nonatomic, retain) NSNumber * max_uses;
@property (nonatomic, retain) NSNumber * max_uses_per_day;
@property (nonatomic, retain) NSString * product_UPCs;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *actions;
@property (nonatomic, retain) NSSet *barcodes;
@property (nonatomic, retain) NSSet *stores;
@end

@interface Ad (CoreDataGeneratedAccessors)

- (void)addActionsObject:(Action *)value;
- (void)removeActionsObject:(Action *)value;
- (void)addActions:(NSSet *)values;
- (void)removeActions:(NSSet *)values;

- (void)addBarcodesObject:(BarCode *)value;
- (void)removeBarcodesObject:(BarCode *)value;
- (void)addBarcodes:(NSSet *)values;
- (void)removeBarcodes:(NSSet *)values;

- (void)addStoresObject:(Store *)value;
- (void)removeStoresObject:(Store *)value;
- (void)addStores:(NSSet *)values;
- (void)removeStores:(NSSet *)values;

@end
