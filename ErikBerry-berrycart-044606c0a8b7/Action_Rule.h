//
//  Action_Rule.h
//  BerryCart
//
//  Created by Romeo Flauta on 2/3/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action, Action_Survey_Answer;

@interface Action_Rule : NSManagedObject

@property (nonatomic, retain) NSNumber * action_id;
@property (nonatomic, retain) NSNumber * action_rule;
@property (nonatomic, retain) NSString * action_rule_description;
@property (nonatomic, retain) NSString * fb_caption;
@property (nonatomic, retain) NSString * fb_image;
@property (nonatomic, retain) NSString * fb_link;
@property (nonatomic, retain) NSString * fb_name;
@property (nonatomic, retain) NSString * question;
@property (nonatomic, retain) NSNumber * shuffle_flag;
@property (nonatomic, retain) NSNumber * survey_type;
@property (nonatomic, retain) NSString * video_thumbnail;
@property (nonatomic, retain) NSString * video_url;
@property (nonatomic, retain) Action *action;
@property (nonatomic, retain) NSSet *action_survey_answers;
@end

@interface Action_Rule (CoreDataGeneratedAccessors)

- (void)addAction_survey_answersObject:(Action_Survey_Answer *)value;
- (void)removeAction_survey_answersObject:(Action_Survey_Answer *)value;
- (void)addAction_survey_answers:(NSSet *)values;
- (void)removeAction_survey_answers:(NSSet *)values;

@end
