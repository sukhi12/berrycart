//
//  BerryCart_API.m
//  BerryCart
//
//  Created by kristopher Romeo Flauta on 1/17/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import "BerryCart_API.h"
#import "AppDelegate.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "AFNetworking.h"
#import "AFURLConnectionOperation.h"
#import "JSON.h"

#import "OpenUDID.h"
#import "XMLReader.h"
#import "MBProgressHUD.h"
#import "TimeStamp.h"
#import "UpdateAllData.h"

NSOperationQueue *queueForID;
NSOperationQueue *queue;
NSOperationQueue *queueForAdID;
NSOperationQueue *queueForAd;
NSString *added_Session;
TimeStamp *timestamp;
NSString *facebookID;
BOOL isfacebookIDRegistered;
NSDateFormatter *unixFormatter;


@implementation BerryCart_API

+(void)startBerryWebService{
    queue = [[NSOperationQueue alloc]init];
    queueForAd = [[NSOperationQueue alloc]init];
    queueForAdID = [[NSOperationQueue alloc]init];
    queueForID = [[NSOperationQueue alloc]init];
    //2014-06-20 04:18:25

    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TimeStamp" inManagedObjectContext:theAppDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [theAppDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count == 0) {
        AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
        NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/timestamp.json" parameters:nil];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSDictionary *timeStamp = [operation.responseString JSONValue];

            TimeStamp *addTimeStmap = [NSEntityDescription insertNewObjectForEntityForName:@"TimeStamp" inManagedObjectContext:theAppDelegate.managedObjectContext];
            addTimeStmap.server_time = [NSString stringWithFormat:@"%@",timeStamp[@"timestamp"]];
            timestamp = addTimeStmap;
            [theAppDelegate saveContext];
            
            [UpdateAllData startupdateAllData];
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
        
        
        [queue addOperation:operation];

    }else{
        timestamp = fetchedObjects[0];
    }
    
}

+(void)addUserSession:(NSString *)session{
    
    added_Session = session;
}
+(void)addFacebookID:(NSString *)fbID{
    facebookID =fbID;
}



#pragma mark POST METHOD!

//GetCash
+(void)GetPendingAndEarnedcompletion:(void (^)(NSDictionary * resul))block{

    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"POST" path:@"/api.bcteston/index.php/api/user_cash.json" parameters:@{@"session": [BerryCart_API user_session]}];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue];
         
         block(dict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         
     }
     ];
    
    [queue addOperation:operation];
    
    
}

+(void)sendActionProofs:(NSDictionary *)params withArrayOfImage:(NSArray *)images withIdentifier:(NSString *)indentifier completion:(void (^)(NSDictionary *dict))block{
    
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
  //  NSMutableURLRequest *myRequest = [client requestWithMethod:@"POST" path:@"/api.bcteston/index.php/api/action_completion_proofs.json" parameters:params];
    
    NSMutableURLRequest *myRequest = [client multipartFormRequestWithMethod:@"POST" path:@"/api.bcteston/index.php/api/action_completion_proofs.json" parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if ([indentifier isEqualToString:@"reciept"]) {
            [images enumerateObjectsUsingBlock:^(UIImage *oneImage, NSUInteger idx, BOOL *stop) {
                
                [formData appendPartWithFileData:UIImageJPEGRepresentation(oneImage, 0.8) name:@"receipt[]" fileName:@"image1.jpg" mimeType:@"image/jpeg"];
            }];
        }
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
       
        NSLog(@"totalBytesExpectedToWrite %lld  ==  %lld",totalBytesExpectedToWrite,totalBytesWritten);
        
    }];

    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue];
         
         block(dict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {\
         [MBProgressHUD hideHUD];
         
     }
     ];
    
    [queue addOperation:operation];
    
}

+(void)completeAction:(NSDictionary *)params completion:(void (^)(NSDictionary *dict))block{
    

    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"POST" path:@"/api.bcteston/index.php/api/complete_action.json" parameters:params];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue];
         
         block(dict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         
     }
     ];
    
    [queue addOperation:operation];
    
    
}






#pragma mark login
+(void)loginAction:(NSDictionary *)params  completion:(void (^)(NSDictionary * resul))block{
    
    
    NSLog(@"params LOGIN :%@", params);
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"POST" path:@"/api.bcteston/index.php/api/login.json" parameters:params];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         NSDictionary *dict = [operation.responseString JSONValue];
         
         NSLog(@"dicttttttt :%@", dict);
         
         theAppDelegate.loggedInUser = dict;
         
         block(dict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         //remove all HUD current view
         [MBProgressHUD hideHUD];
         
         NSDictionary *dict = [operation.responseString JSONValue];
         
         NSLog(@"login failure message: %@", dict[@"message"]);
         
         if ([[NSString stringWithFormat:@"%@",dict[@"message"]] isEqualToString:@"invalid pin"]) {
             
             UIAlertView *invalidPassword = [[UIAlertView alloc]initWithTitle:@"Invalid Password" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
             
             [invalidPassword show];
         }
         
         if ([[NSString stringWithFormat:@"%@",dict[@"message"]] isEqualToString:@"email does not exist"]) {
             
             UIAlertView *invalidPassword = [[UIAlertView alloc]initWithTitle:@"Account Does Not Exist" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
             
             [invalidPassword show];
         }
         
         
         if ([[NSString stringWithFormat:@"%@",dict[@"message"]] isEqualToString:@"customer does not exist"]) {
             
             isfacebookIDRegistered = YES;
             
         }
         
         
         
         block(dict);
         
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queue addOperation:operation];
    //{"error":"invalid pin"}
}

#pragma mark Create_session
+(void)createSessionAction:(void (^)(BOOL hasSession))block{
    
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"POST" path:@"/api.bcteston/index.php/api/create_session.json" parameters:[NSDictionary dictionaryWithObjects:@[@"create_session", [BerryCart_API getUDID], @"1081"] forKeys:@[@"action", @"device", @"store"]]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue];
                  
         [BerryCart_API addUserSession:dict[@"data"][@"SessionID"]];
         block(YES);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideHUD]; 
         
     }
     ];
    
    [queue addOperation:operation];
    
}

#pragma mark register_device
+(void)registerDeviceAction:(void (^)(BOOL isRegistered))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"POST" path:@"/api.bcteston/index.php/api/register_device.json" parameters:@{@"device_id":@"12312321s/s/s/",@"store_id":@"1081",@"device_name":@"name",@"software_version":@"56"}];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue];
         
         if ([[NSString stringWithFormat:@"%@",dict[@"message"]] isEqualToString:@"success"]) {
             block(YES);
         }
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSDictionary *dict = [operation.responseString JSONValue];
         
         if ([[NSString stringWithFormat:@"%@",dict[@"message"]] isEqualToString:@"device_id already exists"]) {
             block(YES);
         }
         
         
         
     }
     ];
    [queue addOperation:operation];
}

#pragma mark - GET METHOD

//check the action
+(void)checkactionByUserID:(NSString *)uid  completaion:(void(^)(NSDictionary *dict))block{
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/action_completions.json" parameters:@{@"user_id": uid}];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue ];
         
         block(dict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queue addOperation:operation];
    
}





#pragma mark getget_sync_data
+(void)syncDataeAction:(NSDictionary *)params  completion:(void (^)(NSDictionary * resul))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/ads.json" parameters:params];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue ];
         
         block(dict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queue addOperation:operation];
}

#pragma mark get barcodes
+(void)getBarcodes:(NSDictionary *)params  completion:(void (^)(NSDictionary * resul))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/ad_barcodes_map.json" parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *dict = [operation.responseString JSONValue ];
         
         block(dict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queue addOperation:operation];
}

#pragma mark Update Stores
+(void)updateStoresAction:(void (^)(NSDictionary * resul))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/stores.json" parameters:@{@"timestamp": @"123123"}];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSError *error = nil;
         NSDictionary *xmlTODict = [XMLReader dictionaryForXMLString:operation.responseString error:&error];
         
         block(xmlTODict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queue addOperation:operation];

}

#pragma mark Update adsStoremap
+(void)updateADSStoreMApAction:(void (^)(NSDictionary * resul))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/ad_store_map.json" parameters:@{@"timestamp": timestamp.server_time}];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSError *error = nil;
         NSDictionary *xmlTODict = [XMLReader dictionaryForXMLString:operation.responseString error:&error];
         
         block(xmlTODict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queue addOperation:operation];

}


+(void)updateAction:(void (^)(NSDictionary * resul))block{
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/actions" parameters:@{@"timestamp": timestamp.server_time}];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSError *error = nil;
         NSDictionary *xmlTODict = [XMLReader dictionaryForXMLString:operation.responseString error:&error];
         
         block(xmlTODict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queue addOperation:operation];

}


//update Ad/Deals

+(void)updateAdOrDealsWithLongLat:(CLLocation *)coordinate completion:(void (^)(NSDictionary * resul))block{

    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    
   //NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/ads.json" parameters:nil];
    
//    NSLog(@"session: %@", [BerryCart_API user_session]);
    //NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/ads_location.json" parameters:@{@"lat":[NSString stringWithFormat:@"%f",coordinate.coordinate.latitude],@"lng":[NSString stringWithFormat:@"%f",coordinate.coordinate.longitude],@"session":[BerryCart_API user_session]}];
//
//    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/ads_location.json" parameters:@{@"lat":@"34.157769", @"lng": @"-118.414074",  @"session":[BerryCart_API user_session]}];
    
//    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/ads_location.json" parameters:@{@"lat":@"14.169912", @"lng": @"121.244063",  @"session":[BerryCart_API user_session]}];

NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/ads.json" parameters:nil];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         NSDictionary *dict = [operation.responseString JSONValue];
        // timestamp.server_time =  dict[@"server_timestamp"];
        //[theAppDelegate saveContext];
         block(dict);
 
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queueForAd addOperation:operation];

}


+(void)getBarCodes:(void (^)(NSDictionary * resul))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/api.bcteston/index.php/api/ad_barcodes_map.json" parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSDictionary *xmlTODict = [operation.responseString JSONValue];
         
         block(xmlTODict);
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queue addOperation:operation];
    
}

#pragma mark -  get ad by ID

+(void)getAdByID:(NSString *)adID completaion:(void (^)(NSDictionary *dict))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    
    
    
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:[NSString stringWithFormat:@"/api.bcteston/index.php/api/ad/%@.json",adID] parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         NSDictionary *rdict = [operation.responseString JSONValue];


         block(rdict);
         
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queueForAdID addOperation:operation];
    
}

+(void)getBarcodeByID:(NSString *)barcode_id completaion:(void (^)(NSDictionary *dict))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];

    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:[NSString stringWithFormat:@"/api.bcteston/index.php/api/barcode/%@.json",barcode_id] parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         NSDictionary *rdict = [operation.responseString JSONValue];
         
         
         block(rdict);
         
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queueForID addOperation:operation];
    
}
+(void)getStoreByID:(NSString *)store_id completaion:(void (^)(NSDictionary *dict))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:[NSString stringWithFormat:@"/api.bcteston/index.php/api/store/%@.json",store_id] parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         NSDictionary *rdict = [operation.responseString JSONValue];
         
         
         block(rdict);
         
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queueForID addOperation:operation];
    
}
+(void)getAdStoreMapByID:(NSString *)ad_store_map_id completaion:(void (^)(NSDictionary *dict))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:[NSString stringWithFormat:@"/api.bcteston/index.php/api/add_store_map/%@.json",ad_store_map_id] parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         NSDictionary *rdict = [operation.responseString JSONValue];
         
         
         block(rdict);
         
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queueForID addOperation:operation];
    
}
+(void)getActionRuleByID:(NSString *)action_rule_id completaion:(void (^)(NSDictionary *dict))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:[NSString stringWithFormat:@"/api.bcteston/index.php/api/action_rule/%@.json",action_rule_id] parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         NSDictionary *rdict = [operation.responseString JSONValue];
         
         
         block(rdict);
         
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queueForID addOperation:operation];
    
}
+(void)getActionSurveyAnswerByID:(NSString *)action_survey_answer_id completaion:(void (^)(NSDictionary *dict))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:[NSString stringWithFormat:@"/api.bcteston/index.php/api/action_survey_answer/%@.json",action_survey_answer_id] parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         NSDictionary *rdict = [operation.responseString JSONValue];
         
         
         block(rdict);
         
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queueForID addOperation:operation];
}
+(void)getActionByID:(NSString *)action_id completaion:(void (^)(NSDictionary *dict))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    
    //berrycart
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:[NSString stringWithFormat:@"/api.bcteston/index.php/api/action/%@.json",action_id] parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation
     setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         NSDictionary *rdict = [operation.responseString JSONValue];
         
         
         block(rdict);
         
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [MBProgressHUD hideHUD];
         NSLog(@"ERROR :%@", error.localizedDescription);
         
     }
     ];
    
    [queueForID addOperation:operation];
}




#pragma mark - Receipt

+(void)getReceiptWithParameters:(NSMutableDictionary *)parameters :(void (^)(NSDictionary * resul))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"POST" path:@"/api.bcteston/index.php/api/receipt" parameters:parameters];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        block([[NSString stringWithFormat:@"%@}",operation.responseString] JSONValue]);
        
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         [MBProgressHUD hideHUD];
                                         NSLog(@"ERROR :%@", error.localizedDescription);
                                         
                                     }
     ];
    
    [queue addOperation:operation];

}

//https://bctest1.com/staging/webservices/?action=pay_amount&session=1389339170-95666f68658dce790c63cd95fab0fd60&transfer_amt=500

#pragma mark - PayPal

+(void)transferFundToPayPal:(NSDictionary *)parameters :(void (^)(NSDictionary * resul))block{
    
    NSLog(@"parameters :%@", parameters);

    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:@"https://bctest1.com"]];
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"/staging/webservices/" parameters:parameters];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"operation.response :%@", operation.response.URL);
        
        block([[NSString stringWithFormat:@"%@}",operation.responseString] JSONValue]);
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideHUD];
        NSLog(@"operation.responseERROR :%@", operation.response.URL);

        NSLog(@"ERROR :%@", error.localizedDescription);
        
    }
    ];
    
    [queue addOperation:operation];
}



#pragma mark - User

+(void)updateUser:(NSDictionary *)parameters :(void (^)(NSDictionary * resul))block{
    
    NSLog(@"parameters :%@", parameters);
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    
    NSString *path = [NSString stringWithFormat:@"/api.bcteston/index.php/api/customer/%@.json", theAppDelegate.loggedInUser[@"data"][@"Debug"][@"CustomerId"]];
    
    //NSDictionary *paramTEsting = [NSDictionary dictionaryWithObject:@"123456789" forKey:@"facebook_id"];
    
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"PUT" path:path parameters:parameters];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"operation :%@", operation.response.URL);
        
        NSDictionary *returnDict = [operation.responseString JSONValue];
        
        if([[NSString stringWithFormat:@"%@",returnDict[@"message"]] isEqualToString:@"success"]){
            block(returnDict);
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideHUD];
        NSDictionary *returnDict = [operation.responseString JSONValue];
        
        if([[NSString stringWithFormat:@"%@",returnDict[@"message"]] isEqualToString:@"success"]){
            block(returnDict);
            
        }
        
    }
     ];
    
    [queue addOperation:operation];
}

+(void)createNewUser:(NSDictionary *)parameters :(void (^)(NSDictionary * resul))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    NSMutableURLRequest *myRequest = [client requestWithMethod:@"POST" path:@"/api.bcteston/index.php/api/create_account.json" parameters:parameters];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary *returnDict = [operation.responseString JSONValue];
        
        if([[NSString stringWithFormat:@"%@",returnDict[@"message"]] isEqualToString:@"success"]){
            block(returnDict);
            
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [MBProgressHUD hideHUD];
        
        NSDictionary *returnDict = [operation.responseString JSONValue];
        block(returnDict);
        if([[NSString stringWithFormat:@"%@",returnDict[@"message"]] isEqualToString:@"success"]){
            block(returnDict);
            
        }
    }
     ];
    
    [queue addOperation:operation];
}

+(void)getUserData:(NSDictionary *)parameters :(void (^)(NSDictionary * resul))block{
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:base_url]];
    
    NSString *path = [NSString stringWithFormat:@"/api.bcteston/index.php/api/customer/%@.json", theAppDelegate.loggedInUser[@"data"][@"Debug"][@"CustomerId"]];

    NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:path parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]initWithRequest:myRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *response = [operation.responseString JSONValue];
        
        block(response);
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [MBProgressHUD hideHUD];
        NSLog (@"FAIL :%@", error.localizedDescription);
        
        NSDictionary *response = [operation.responseString JSONValue];
        
        block(response);
        
    }
     ];
    
    [queue addOperation:operation];
}



#pragma  mark - CLLocation



#pragma mark - Other

+(NSString *)getUDID{
    return [OpenUDID value];
}

+(NSString *)user_session{
    
    return added_Session;
}
+(NSString *)facebookID{
    return facebookID;
}

+(BOOL)facebookIDisNotregistered{
    return isfacebookIDRegistered;
}




@end