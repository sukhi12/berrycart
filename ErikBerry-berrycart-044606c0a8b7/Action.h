//
//  Action.h
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action_Rule, Ad;

@interface Action : NSManagedObject

@property (nonatomic, retain) NSNumber * action_id;
@property (nonatomic, retain) NSString * action_type;
@property (nonatomic, retain) NSNumber * ad_id;
@property (nonatomic, retain) NSString * fb_description;
@property (nonatomic, retain) NSString * fb_link;
@property (nonatomic, retain) NSString * fb_name;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * line1;
@property (nonatomic, retain) NSNumber * max_uses;
@property (nonatomic, retain) NSNumber * max_uses_per_day;
@property (nonatomic, retain) NSString * offer_value;
@property (nonatomic, retain) NSString * question;
@property (nonatomic, retain) NSString * shuffle_flag;
@property (nonatomic, retain) NSString * survey_type;
@property (nonatomic, retain) NSString * validation_required;
@property (nonatomic, retain) NSString * video_url;
@property (nonatomic, retain) Ad *ad;
@property (nonatomic, retain) Action_Rule *action_rule;

@end
