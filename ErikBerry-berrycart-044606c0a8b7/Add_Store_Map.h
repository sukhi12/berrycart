//
//  Add_Store_Map.h
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Add_Store_Map : NSManagedObject

@property (nonatomic, retain) NSNumber * ad_id;
@property (nonatomic, retain) NSNumber * ad_store_map_id;
@property (nonatomic, retain) NSNumber * assigned_brand_id;
@property (nonatomic, retain) NSNumber * store_id;

@end
