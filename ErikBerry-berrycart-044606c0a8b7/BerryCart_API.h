//
//  BerryCart_API.h
//  BerryCart
//
//  Created by kristopher Romeo Flauta on 1/17/14.
//  Copyright (c) 2014 BerryCart. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;


#define base_url @"http://bctest1.com"
//192.1068.1.43/api.bcteston/index.php/api/login.json
//register_device
///api.bcteston/index.php/api/stores/
//ad_store_map

//get server time stamp
//http://192.168.1.43/api.bcteston/index.php/api/timestamp.json

@interface BerryCart_API : NSObject

//XML READER
//https://github.com/amarcadet/XMLReader


+(void)startBerryWebService;
+(NSString *)getUDID;
+(NSString *)facebookID;
+(NSString *)user_session;
+(BOOL)facebookIDisNotregistered;
+(void)addUserSession:(NSString *)session;
+(void)addFacebookID:(NSString *)session;
#pragma mark - POST METHOD


//GetCash
+(void)GetPendingAndEarnedcompletion:(void (^)(NSDictionary * resul))block;
//login
+(void)loginAction:(NSDictionary *)params  completion:(void (^)(NSDictionary * resul))block;
//Create_session
+(void)createSessionAction:(void (^)(BOOL hasSession))block;
//register_device
+(void)registerDeviceAction:(void (^)(BOOL isRegistered))block;
//to complete action
+(void)completeAction:(NSDictionary *)params completion:(void (^)(NSDictionary *dict))block;
//to send proofs of action
+(void)sendActionProofs:(NSDictionary *)params withArrayOfImage:(NSArray *)images withIdentifier:(NSString *)indentifier completion:(void (^)(NSDictionary *dict))block;



#pragma mark - GET METHOD
//active store by action_id;
//check the action
+(void)checkactionByUserID:(NSString *)uid  completaion:(void(^)(NSDictionary *dict))block;
//sync_data
+(void)syncDataeAction:(NSDictionary *)params  completion:(void (^)(NSDictionary * resul))block;
//update deals
+(void)updateAdOrDealsWithLongLat:(CLLocation *)coordinate completion:(void (^)(NSDictionary * resul))block;
//Update Stores
+(void)updateStoresAction:(void (^)(NSDictionary * resul))block;
//Update adsStoremap
+(void)updateADSStoreMApAction:(void (^)(NSDictionary * resul))block;
//update action
+(void)updateAction:(void (^)(NSDictionary * resul))block;
//Barcode
+(void)getBarCodes:(void (^)(NSDictionary * resul))block;


+(void)getAdByID:(NSString *)adID completaion:(void (^)(NSDictionary *dict))block;
+(void)getBarcodeByID:(NSString *)barcode_id completaion:(void (^)(NSDictionary *dict))block;
+(void)getStoreByID:(NSString *)store_id completaion:(void (^)(NSDictionary *dict))block;
+(void)getAdStoreMapByID:(NSString *)ad_store_map_id completaion:(void (^)(NSDictionary *dict))block;
+(void)getActionRuleByID:(NSString *)action_rule_id completaion:(void (^)(NSDictionary *dict))block;
+(void)getActionSurveyAnswerByID:(NSString *)action_survey_answer_id completaion:(void (^)(NSDictionary *dict))block;
+(void)getActionByID:(NSString *)action_id completaion:(void (^)(NSDictionary *dict))block;






//check the udid if already registered

+(void)transferFundToPayPal:(NSMutableDictionary *)parameters :(void (^)(NSDictionary * resul))block;
+(void)createNewUser:(NSDictionary *)parameters :(void (^)(NSDictionary * resul))block;
+(void)updateUser:(NSDictionary *)parameters :(void (^)(NSDictionary * resul))block;
+(void)getUserData:(NSDictionary *)parameters :(void (^)(NSDictionary * resul))block;



//fetch





@end
