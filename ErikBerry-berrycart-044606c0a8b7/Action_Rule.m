//
//  Action_Rule.m
//  BerryCart
//
//  Created by Romeo Flauta on 2/3/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import "Action_Rule.h"
#import "Action.h"
#import "Action_Survey_Answer.h"


@implementation Action_Rule

@dynamic action_id;
@dynamic action_rule;
@dynamic action_rule_description;
@dynamic fb_caption;
@dynamic fb_image;
@dynamic fb_link;
@dynamic fb_name;
@dynamic question;
@dynamic shuffle_flag;
@dynamic survey_type;
@dynamic video_thumbnail;
@dynamic video_url;
@dynamic action;
@dynamic action_survey_answers;

@end
