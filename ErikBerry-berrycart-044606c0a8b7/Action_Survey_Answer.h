//
//  Action_Survey_Answer.h
//  BerryCart
//
//  Created by Romeo Flauta on 2/2/14.
//  Copyright (c) 2014 Erik Berry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action_Rule;

@interface Action_Survey_Answer : NSManagedObject

@property (nonatomic, retain) NSNumber * action_rule_id;
@property (nonatomic, retain) NSNumber * action_survey_answer_id;
@property (nonatomic, retain) NSString * answer;
@property (nonatomic, retain) Action_Rule *action_rule;

@end
