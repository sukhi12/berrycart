//
//  BerryCart_CoreData.h
//  BerryCart
//
//  Created by Romeo Flauta on 1/24/14.
//  Copyright (c) 2014 BerrryCart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface BerryCart_CoreData : NSObject

+(void)fetchObjectsWithoutPredicate:(NSString *)entityName  completion:(void (^)(NSArray * resul))block;
+ (void)fetchObjectsWithPredicate:(NSString *)entityName andPredicateString:(NSString *)predicateString andPredicateArgument: (NSString *) predicateArgument completion:(void (^)(NSArray * result))block;

@end
